// Domaca naloga 2 : 
// Naredi funkcijo, ki izracuna, kako dolgo bo trajalo preden kamen pade na tla. 
// Kamen vrzemo  v zrak, navpicno navzgor s hitrostjo v0 (ki je parameter funkcije)

// Privzamemo : 
// g = 10 (tezni pospesek)
// Zracnega upora ni.

#include <stdio.h>
#include <math.h>

float cas_padanja( float v0 ) 
{
	float g = 10;
	// Enacba za cas dviganja : 
	// v0 = g * t
	// Cas padanja pa je enak casu dviganja 
	// Sledi : 
	// t = 2 * v0 / g
	return 2 * v0 / g;
}

int main( void ) 
{
	float v0; 
	v0 = 5;
	printf("Ce vrzem kamen navzgor s hitrostjo %f m/s, potem pade nazaj po %f s.\n", v0, cas_padanja(v0) );

	v0 = 10;
	printf("Ce vrzem kamen navzgor s hitrostjo %f m/s, potem pade nazaj po %f s.\n", v0, cas_padanja(v0) );

	v0 = 20;
	printf("Ce vrzem kamen navzgor s hitrostjo %f m/s, potem pade nazaj po %f s.\n", v0, cas_padanja(v0) );


	// Najhitrejsi pitch pri baseballu : 106 mph = 47.39
	// https://www.youtube.com/watch?v=ngubly4hpHw
	v0 = 47.39;
	printf("(Baseball : najhitrejsi met)\n");
	printf("Ce vrzem kamen navzgor s hitrostjo %f m/s, potem pade nazaj po %f s.\n", v0, cas_padanja(v0) );
	
	// Servis pri tenisu : 73.17 m/s
	// https://en.wikipedia.org/wiki/Fastest_recorded_tennis_serves
	v0 = 73.17;
	printf("(Tenis : najhitrejsi servis)\n");
	printf("Ce vrzem kamen navzgor s hitrostjo %f m/s, potem pade nazaj po %f s.\n", v0, cas_padanja(v0) );
	//
	// Brcanje zoge pri nogometu : 14.9 m/s
	// http://www.livestrong.com/article/441962-how-much-force-does-an-average-soccer-player-kick-the-ball-with/
	v0 = 14.9;
	printf("(Nogomet : povprecen igralec)\n");
	printf("Ce vrzem kamen navzgor s hitrostjo %f m/s, potem pade nazaj po %f s.\n", v0, cas_padanja(v0) ); 

	return 0;
}
