#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>


// Funkcije od prejsnic : 

int vsota( int a, int b ) 
{
	return a+b;
}

void razlicneOperacije( int a, int b )
{
	printf("%d + %d = %d\n", a, b, a+b );
	printf("%d - %d = %d\n", a, b, a-b );
	printf("%d * %d = %d\n", a, b, a*b );
	printf("%d / %d = %d\n", a, b, a/b );
}

// Nove funkcije : 
// Naloge : 
// Napisite funkcijo, ki 
//	- vrne vsoto sin(x) + cos(x)

float vsota_sin_cos( float x ) 
{
	return sin(x) + cos(x); 
}

// https://github.com/matjaz1234/vajeRP

//	- vrne exp(-x), hkrati pa izpise na zaslon vrednost exp( +x )

float vaja_exp( float x )
{
	// printf( ... ??? )
	// return (...) ?
	printf("FUNKCIJA : exp( + %f ) = %f\n", x, exp(x) );
	return exp( - x );
}

//	- prebere vrednost realnega stevila od uporabnika, na zaslon pa izpise kvadrat prebranega stevila 
void preberi_izpisi_kvadrat( void )
{
	float x;
	printf("Vpisite realno stevilo : ");
	scanf("%f", &x );
	printf("%f ^ 2 = %f\n", x, x*x );
}
//	- izracuna polinom 0*x + -3 * x + 7 * x^2 + 22 * x^3
float polinom1( float x )
{
	return 0*x + -3 * x + 7 * x * x + 22 * x * x * x;
}

//	- izracuna polinom 12*x + 3 * x + 17 * x^2 + 2 * x^3
float polinom2( float x )
{
	return 12*x + 3 * x + 17 * x * x + 2 * x * x * x;
}
//	- vrne nakljucno celo stevilo v danem intervalu (namig : za poljubno nakljucno stevilo uporabi funkcijo rand() )
int nakljucno_stevilo_v_intervalu( int min_x, int max_x ) 
{
	// Vrnemo lahko min_x, min_x +1, ... max_x-1
	return ( rand() % ( max_x - min_x ) ) + min_x;
}

//	- vrne stevilo v sredini treh stevil, podanih kot argumenti
float sredina_treh( float x, float y, float z )
{
	if( ( x <= y <= z ) 
			|| ( z <= y <= x ) )
		return y; 
	else if ( ( y <= x <=z ) ||
			( z <= x <= y ) )
		return x;
	else if( ( x <= z <= y ) || 
			( y <= z <= x ) )
		return z;
}

// Sestavi funkcijo, ki za dva pogoja (stevili 0 ali 1 ) izpise tabelo resnicnosti operatorjev 
// || (or), && (and)
void izpisi_resnicnost( int x, int y )
{
	printf("x||y =");
	if( x || y )
		printf("1");
	else
		printf("0");
	printf("\n");

	printf("x&&y =");
	if( x && y )
		printf("1");
	else
		printf("0");
	printf("\n");
}


int main(void) 
{
	srand( time(NULL) );

	int i;
	float x;
	x = 3.0;
	printf("Vsota : sin(%f) + cos(%f) = %f\n", x, x, vsota_sin_cos(x) );

	printf("Funkcija exp : x = %f\n", x);
	float y;
	y = vaja_exp( x );
	printf("Exp(-x) = %f\n", y );

	// printf("Funkcija preberi_izpisi_kvadrat : \n");
	// preberi_izpisi_kvadrat();

	printf("Funkcija polinom1 : \n");
	x = 1.2;
	printf("polinom1( %f ) = %f\n", x, polinom1(x) );
	printf("polinom2( %f ) = %f\n", x, polinom2(x) );
	x = 22.2;
	printf("polinom1( %f ) = %f\n", x, polinom1(x) );
	printf("polinom2( %f ) = %f\n", x, polinom2(x) );

	printf("Nakljucno stevilo v intervalu :\n");
	int min_x = 10;
	int max_x = 15;
	for( i=0; i<10; ++i )
		printf("Nakljucno stevilo [ %d, %d ) = %d\n", min_x, max_x, nakljucno_stevilo_v_intervalu( min_x, max_x ) );

	printf("Sredina treh stevil\n");
	float z;
	x = 1.0; y = 2.0; z = 5.0;
	printf("Sredina %f, %f, %f = %f \n", x, y, z, sredina_treh(x, y, z) );
	x = 100.0; y = 20.0; z = 5.0;
	printf("Sredina %f, %f, %f = %f \n", x, y, z, sredina_treh(x, y, z) );

	int j;
	for( i=0; i<2; ++i )
		for( j=0; j<2; ++j )
		{
			printf("i = %d, j = %d\n", i, j );
			izpisi_resnicnost(i,j);
		}

	return 0;
}
