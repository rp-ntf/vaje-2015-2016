/*
 * Ponavljanje : 
 * - funkcije 
 * - kazalci 
 */

#include <stdio.h>
#include <string.h>

// Domača naloga (OBVEZNA) 1 : 
// Napiši funkcije, ki zamenjajo dve spremenljivki tipov 
// int, float, double, char, 
//
// Domača naloga (OBVEZNA) 2 :
// Napiši funkcije, ki izpišejo tabele tipov 
// int, float, double, char, 

/*
 * Naloge : 
 * 1. Sestavite funkcijo, ki zamenja dolocen znak z drugim znakomv v podanem nizu 
 * Pri implementaciji poisci vse znake s pomocjo strchr funkcije, in zamenjaj najdene znake s pomocjo kazalcev.
 */

void zamenjaj_znake( char * niz, int n, char znak_orig, char znak_zamenjaj )
{
	char * p;
	do {
		p = strchr( niz, znak_orig );
		if ( p != NULL ) 
			*p = znak_zamenjaj;
	} while ( p != NULL );
}

// Funkcija, ki izpise celo tabelo :
void izpisi_tabelo( int tabela[], int n )
{
	int i; 
	for( i=0; i<n; ++i )
	{
		printf("%d | %d\n", i, tabela[i] );
	}
}

void izpisi_tabelo_float( float tabela[], int n )
{
	int i; 
	for( i=0; i<n; ++i )
	{
		printf("%d | %f\n", i, tabela[i] );
	}
}

// Funkcija, ki zamenja dve realni stevili (float)
void zamenjaj_realni_stevili( float * kazalec_x, float * kazalec_y )
{
	// printf("Kam kaze kazalec x : %ld\n", kazalec_x );
	// printf("Kam kaze kazalec y : %ld\n", kazalec_y );
	// printf("Kaj je tam, kamor kaze kazalec x : %f\n", *kazalec_x );
	// printf("Kaj je tam, kamor kaze kazalec y : %f\n", *kazalec_y );
	float z;
	z = *kazalec_x;
	*kazalec_x = *kazalec_y;
	*kazalec_y = z;
}

// Zamenjaj vse elemente dveh tabel 
// void zamenjaj_tabeli( float * tabela_x, float * tabela_y, int n )
void zamenjaj_tabeli( float tabela_x[], float tabela_y[], int n )
{
	float z;
	int i;
	for( i=0; i<n; ++i )
	{
		z = tabela_x[i];
		tabela_x[i] = tabela_y[i];
		tabela_y[i] = z;
	}
}
 
// Sestavi funkcijo, ki izracuna vsoto kvadratov tabele celih stevil, in vrne to vsoto.
int vsota_kvadratov( int tab[], int n )
{
	int vsota = 0;
	int i;
	for(i = 0; i<n; ++i )
	{
		vsota += tab[i] * tab[i];
	}
	return vsota;
}

// Vrne vsoto preko kazalca 
// Vrne vsoto kvadratov in se navadno vsoto
void vsota_kvadratov_kazalec( int tab[], int n, int * kazalec_vsota, int * kazalec_navadna_vsota )
{
	*kazalec_vsota = 0;
	*kazalec_navadna_vsota = 0;
	int i;
	for(i = 0; i<n; ++i )
	{
		*kazalec_vsota += tab[i] * tab[i];
		*kazalec_navadna_vsota += tab[i];
	}
}

// Funkcija, ki uredi dva elementa po velikosti : 
void uredi( int * kazalec_a, int * kazalec_b )
{
	// Uredi a, b po velikosti, tako da je a < b.
	int c;
	if( *kazalec_a > *kazalec_b )
	{
		printf("a > b\n");
		c = *kazalec_b;
		*kazalec_b = *kazalec_a;
		*kazalec_a = c;
	}
}

int main( void ) 
{
	char niz[] = "Marko skace po zeleni travi";
	int n = 28; 
	printf("Niz pred zamenjavo : %s\n", niz );
	printf("Menjam znak \"a\" -> \"#\"\n");
	zamenjaj_znake( niz, n, 'a', '#' );
	printf("Niz po zamenjavi   : %s\n", niz );

	printf("Funkcija zamenjaj realni stevili\n");
	float x, y;
	x = 10; y = 20;
	printf("Pred zamenjavo : x = %f, y = %f\n", x, y );
	zamenjaj_realni_stevili( & x, & y );
	printf("Po zamenjavi   : x = %f, y = %f\n", x, y);

	printf("Zamenjava dveh tabel \n");
	
	float tab_x[] = { 1, 2, 3, };
	float tab_y[] = { 6, 7, 8, };
	n = 3;
	printf("Pred zamenjavo : \n");
	printf(" i | x | y \n");
	int i;
	for( i=0; i<n; ++i )
	{
		printf("%d | %f | %f \n", i, tab_x[i], tab_y[i] );
	}
	zamenjaj_tabeli( tab_x, tab_y, n );
	printf("Po zamenjavi : \n");
	printf(" i | x | y \n");
	for( i=0; i<n; ++i )
	{
		printf("%d | %f | %f \n", i, tab_x[i], tab_y[i] );
	}

	int tabela[] = { 3, 4, 5 };
	int tabela_n = 3;
	izpisi_tabelo( tabela, tabela_n );
	izpisi_tabelo_float( tab_x, n );

	printf("VSOTA tabele :\n");
	int vsota; 
	int navadna_vsota;
	vsota = vsota_kvadratov( tabela, tabela_n );
	printf("Vsota kvadratov = %d\n", vsota );
	vsota_kvadratov_kazalec( tabela, tabela_n, & vsota, & navadna_vsota );
	printf("Vsota kvadratov preko kazalca = %d\n", vsota );
	printf("Navadna vsota preko kazalca = %d\n", navadna_vsota );

	int a = 10;
	int b = 5;
	printf("Pred urejanjem : \n");
	printf("a = %d, b = %d\n", a, b);
	uredi( &a, &b );
	printf("Po urejanju : \n");
	printf("a = %d, b = %d\n", a, b);

	return 0;
}

