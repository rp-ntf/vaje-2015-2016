// Domaca naloga 1 : OBVEZNA
// sestavi funkcijo, ki kot parameter prejme stevilo zvezdic, ki jih izpise na ekrat.
// f(4) izpise
// ****

#include <stdio.h>

void zvezdice( int n )
{
	int i;
	for( i=0; i<n; ++i )
		printf("*");
	printf("\n");
}

int main( void ) 
{
	int n;

	n=5;
	printf("%d zvezdic : \n", n );
	zvezdice(n);

	n=8;
	printf("%d zvezdic : \n", n );
	zvezdice(n);

	n=3;
	printf("%d zvezdic : \n", n );
	zvezdice(n);

	return 0;
}
