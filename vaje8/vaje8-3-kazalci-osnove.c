/*
 * Kazalci : osnove 
 */

#include <stdio.h>
#include <string.h>
#include <math.h>

/*
 * [Najprej glej tekst v glavni funkciji]
 * [1]
 * Funkcija zamenjaj vzame dva kazalca na spremenljivki tipa int, vrne pa ničesar.
 * Ker vzame kot argument kazalec na neko spremenljivko, se zamenjava pozna na prvotnih spremenljivkah.
 */
void zamenjaj( int * kazalec_x, int * kazalec_y )
{
	int z;
	z = *kazalec_x;
	*kazalec_x = * kazalec_y;
	*kazalec_y = z;
}

// Ta funkcija ne deluje kot bi pricakovali
void zamenjaj_brez_kazalcev( int x, int y )
{
	int z;
	z = x;
	x = y; 
	y = z;
}

/*
 * [2]
 * S pomocjo kazalcev lahko iz funkcije vrnemo vec kot eno spremenljivko 
 */
void vsota_razlika( int x, int y, int * kazalec_vsota, int * kazalec_razlika )
{
	*kazalec_vsota = x+y;
	*kazalec_razlika = x - y;
}

/*
	 * Funkcija min_max [3] vrne najvecji in najmanjsi element v tabeli.
	 * Funkcija potence [4] vrne vrednosti x^0, x^1, x^2, ... 
	 */
/*
 * [3]
 * Funkcija min_max vrne najvecji in najmanjsi element v tabeli 
 */
void min_max( int * tab, int n, int * tab_min, int * tab_max )
{
	int i;
	if( n == 0 )
		return;
	*tab_min = tab[0];
	*tab_max = tab[0];
	for( i=1; i<n; ++i )
	{
		if ( tab[i] > *tab_max ) 
			*tab_max = tab[i];
		if ( tab[i] < *tab_min ) 
			*tab_min = tab[i];
	}
}

/*
 * Funkcija potence vrne potence stevila x v tabeli
 */
void potence( int * tab, int n, int x )
{
	int i;
	int potenca = 1;
	for( i=0; i<n; ++i )
	{
		tab[i] = potenca;
		potenca *= x;
	}
}

int main( void)
{
	int MAX = 256;
	int i;
	char niz[MAX];

	/*
	 * Kazalec na spremenljivko je poseben tip spremenljivke, ki nam pove, kje v pomnilniku se nahaja 
	 * spremenljivka, na katero kaze.
	 * 
	 */
	int x;
	int * kazalec_x = &x;
	//  A             A 
	//  L znak (*) pri dekleraciji spremenljivke pomeni, da je spremenljivka kazalec
	//                L znak & pred spremenljivko pa označuje operator, ki 
	//                nam vrne naslov spremenljivke.
	
	// S pomocjo kazalcev na spremenljivko lahko spremenimo vrednost spremenljivke, na katero kazalec kaze
	x = 10;
	printf("x = %d\n", x );
	*kazalec_x = 100;
//  A   
//  L znak (*) pred spremenljivko tipa kazalec, 
//  nam vrne vrednost spremenljivke, na katero kazalec kaže. 
//  Tako lahko vrednost spremenljivke tudi spremenimo.
	printf("x = %d (po nastavitvi preko kazalca)\n", x );

	printf("Nadaljujem ?\n");
	gets(niz);

	// Spremenljivka tipa kazalec se lahko uporablja kot običajne spremenljivke.
	// Lahko jo uporabimo kot argument funkcij.
	// Naslednja funkcija zamenja vrednosti dveh spremenljivk : [1]
	int y;
	y = 30;
	printf("Pred zamenjavo : x = %d, y = %d\n", x, y );
	zamenjaj( &x, &y );
	//        A   A 
	//        L___L__ znak (&) pred spremenljivkama x, y pomeni (naslov spremenljivke) x,y
	//        Naslov spremenljivke tipa (int) pa je kazalec na spremenljivko istega tipa.
	printf("Po zamenjavi   : x = %d, y = %d\n", x, y );
	zamenjaj( &x, &y );
	printf("Po zamenjavi 2 : x = %d, y = %d\n", x, y );
	// poizkusimo zamenjavo se enkrat brez kazalcev : 
	zamenjaj_brez_kazalcev( x, y );
	printf("Po zamenjavi brez kazalcev : x = %d, y = %d\n", x, y );

	printf("Nadaljujem ?\n");
	gets(niz);

	// S pomocjo kazalcev lahko iz funkcije vrnemo tudi vec rezultatov 
	// Funkcija [2] vsota_razlika zapise vrednosti vsote in razlike v ustrezni spremenljivki.
	int vsota;
	int razlika;
	vsota_razlika( x, y, &vsota, &razlika );
	//  Funkciji smo spremenljivki x,y podali kot obicajni spremenljivki (brez kazalcev), 
	//  saj jih funkcija ne bo spreminjala.
	//  Spremenljivki vsota in razlika pa podamo funkciji preko kazalcev, 
	//  da lahko funkcija spremeni vrednosti teh dveh spremenljivk.
	printf("Pred vsoto / razliko : x = %d, y = %d\n", x, y );
	printf("Vsota   : %d + %d = %d\n", x, y, vsota );
	printf("Razlika : %d - %d = %d\n", x, y, razlika );

	printf("Nadaljujem ?\n");
	gets(niz);

	/*
	 * Kazalci in tabele 
	 *
	 * Kazalci so zelo podobni tabelam. 
	 * Tabela je v programu predstavljena s kazalcem na njen zacetek (za to si moramo dolzino tabele zapomniti sami)
	 * */

	char abc[] = "ABCDEFGHIJKL";
	printf("abc kot niz : %s\n", abc );
	printf("abc : %ld\n", abc );
	printf("char : sizeof( abc[0] ) = %ld\n", sizeof( abc[0] ) );

	for( i=0; i<5; ++i )
		printf("abc[%d] : %c \t &abc[%d] = %ld \n", i, abc[i], i, &abc[i] );


	int tab[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	printf("tab = %ld\n", tab );
	printf("int : sizeof(tab[0]) = %ld\n", sizeof(tab[0]) );
	for( i=0; i<5; ++i )
		printf("tab[%d] : %d \t &tab[%d] = %ld \n", i, tab[i], i, &tab[i] );


	double tab2[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	printf("tab2 = %ld\n", tab2 );
	printf("double : sizeof(tab2[0]) = %ld\n", sizeof(tab2[0]) );
	for( i=0; i<5; ++i )
		printf("tab2[%d] : %f \t &tab2[%d] = %ld \n", i, tab2[i], i, &tab2[i] );

	/* 
	 * Operator sizeof( spremenljivka ) nam pove velikost spremenljivke v bytih.
	 */

	printf("Nadaljujem ?\n");
	gets(niz);

	/*
	 * Preko kazalcev pa lahko funkcije sprejmejo kot argument tabelo, 
	 * in tudi vrnejo tabelo. 
	 * Funkcija min_max [3] vrne najvecji in najmanjsi element v tabeli.
	 * Funkcija potence [4] vrne vrednosti x^0, x^1, x^2, ... 
	 */

	int tabela_min_max[10] = { 0, -1, -2, 1, 2, 5, 17, 222, -10, 12 };
	int tab_min, tab_max;
	min_max( tabela_min_max, 10, & tab_min, & tab_max );
	printf("min = %d, max = %d\n", tab_min, tab_max );

	int potence_x[10];
	x = 2;
	potence( potence_x, 10, x );
	for( i=0; i<10; ++i )
	{
		printf("%d ^ %d = %d\n", x, i, potence_x[i] );
	}

	printf("Nadaljujem ?\n");
	gets(niz);

	return 0;
}




