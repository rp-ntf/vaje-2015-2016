/*
 * Tabele : ponavljanje 
 * Napiši program, ki izračuna skalarni produkt dveh vektorjev, 
 * podanih kot tabeli poljubne dolžine.
 *
 */

#include <stdio.h>
#include <math.h>

int main(void)
{
	int i; // števec
	int n; // velikost tabele 
	int MAX = 256; // najvecja dovoljena velikost tabele
	// Deklaracija tabele 
	float a[MAX];
	float b[MAX];
	float sum = 0.0;
	// uporabljena velikost tabele :
	printf("Vnesite stevilo elementov tabele : ");
	scanf("%d", &n);
	if( n > MAX ) // Tabela ne sme biti večja od MAX
		n = MAX;
	for( i=0; i<n; ++i )
	{
		printf("a[%d] = ", i );
		scanf("%f", &a[i] );
	}
	for( i=0; i<n; ++i )
	{
		printf("b[%d] = ", i );
		scanf("%f", &b[i] );
	}

	// v zanki sestej vrednosti a[i]*b[i]
	// Skalarni produkt je koren izračunane vsote.
	sum = 0.0;
	for( i=0; i<n; i++ )
	{
		sum = sum + a[i] * b[i];
	}
	printf("Skalarni produkt je %f.\n", sqrt(sum) );
	
	// Dodatna naloga : 
	// Izracunaj razdaljo med tockami a in b.
	// Razdalja je definirana kot 
	// sqrt( sum( |a[i]-b[i]|^2 ) )
	float zac; // zacasna spremenljivka 
	sum = 0.0;
	for( i=0; i<n; ++i )
	{
		zac = a[i] - b[i];
		sum = sum + zac * zac;
	}
	printf("Razdalja med a in b je %g.\n", sqrt( sum ) );

	return 0; 
}

