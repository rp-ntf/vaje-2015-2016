/*
 * Tabele : ponavljanje 
 * Napiši program, ki uredi tabelo. 
 *
 * Urejanje naj bo narejeno na kar se da enostaven način : 
 * 1. na vsakem koraku algoritma naredimo : 
 *	1a : preverimo, ali je tabela urejena. Če je, prekinemo izvajanje 
 *	1b : gremo v zanki čez celo tabelo, in v primeru da 
 *		tab[i] > tab[i+1] 
 *		zamenjamo elementa i in i+1.
 *	Dodatna vprasanja / naloge : 
 *	- najvec kolikokrat moramo izvrsiti zunanjo zanko ? 
 */

#include <stdio.h>

int main(void)
{
	int i; // števec 
	int tmp; // zacasna spremenljivka za zamenjavo vrednosti
	int n; // velikost tabele 
	int MAX = 256; // najvecja dovoljena velikost tabele
	// Deklaracija tabele 
	int tab[MAX];
	// uporabljena velikost tabele :
	printf("Vnesite stevilo elementov tabele : ");
	scanf("%d", &n);
	if( n > MAX ) // Tabela ne sme biti večja od MAX
		n = MAX;
	for( i=0; i<n; ++i )
	{
		printf("Vnesite %d-to stevilo tabele [%d] : ", i, n);
		scanf("%d", &tab[i] );
	}

	// Zunanja zanka
	int jeUrejena = 0;
	while( jeUrejena == 0 )
	{
		// 1.a : ali je tabela urejena ? Uporabimo enak algoritem kot v prejsnem programu (kopiraj sem)

		jeUrejena = 1;
		for( i=1; i<n; ++i )
		{
			if( !(tab[i-1] <= tab[i]) )
			{
				jeUrejena = 0;
				break;
			}
		}
		if( jeUrejena )
			break;

		// 1.b : v zanki gremo cez celo tabelo 
		for( i=1; i<n; ++i )
		{
			// ce je tab[i] > tab[i+1], potem zamenjamo tab[i] in tab[i+1]
			if( tab[i-1] > tab[i] )
			{
				tmp = tab[i-1];
				tab[i-1] = tab[i];
				tab[i] = tmp;
			}
		}
	}

	// Na koncu izpisemo urejeno tabelo
	printf("Urejena tabela je :\n");
	for( i=0; i<n; ++i )
	{
		printf("tab[%d] = %d\n", i, tab[i] );
	}

	return 0; 
}

