/*
 * Nizi : osnove
 * Naloga : 
 * Sestavi program, ki presteje stevilo besed v vnesenem nizu.
 * Besede so med seboj locene z enim ali vec presledkov.
 * Za vnos 
 * "Marko skace  po zeleni 73 travi"
 * program izpise 6.
 */

#include <stdio.h>
#include <string.h>

int main(void)
{
	int MAX = 256;
	int i;
	int stevec = 0;
	int prejsniJePresledek = 0;
	char niz[MAX];
	// najprej preberemo niz od uporabnika (gets)
	// gets(...) 
	printf("Vnesite niz : ");
	gets(niz);
	printf("Prebrali smo (gets) niz \"%s\".\n", niz);
	//
	// Potem pa v zanki iteriramo cez vse znake v nizu
	// in stejemo, kolikokrat naletimo na skupino presledkov.
	for( i=1; i<strlen(niz); ++i )
	{
		if( niz[i-1] != ' ' && niz[i] == ' ' )
			stevec++;
	}
	if( niz[ strlen(niz) -1 ] != ' ' )
		stevec += 1;
	
	printf("V nizu \"%s\" je %d besed.\n", niz, stevec );

	// Domaca naloga : 
	// Sestavi program, ki bo vsako besedo izpisal v svojo vrstico.
	// Namig : zamenjaj znake ' ' z znaki '\n' 
	// (znak presledek z znakom nova vrstica)

	return 0; 
}

