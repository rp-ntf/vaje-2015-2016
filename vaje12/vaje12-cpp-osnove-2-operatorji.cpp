/*
 * Vaje 12 : C++, operatorji 
 */
#include <iostream>

// Naloge : 
//
// Naloga 1 : 
// Naredite razred, ki predstavlja cas v dnevu (ure, minute, sekunde). 
// Implementirajte operatorje sestevanja in odstevanja pri tem razredu, 
// in preverite da zares pravilno delujejo
//
// Naloga 2 : 
// Naredite razred, ki predstavlja cas v letu (mesec = 30 dni):( dan, mesec, leto )
// Implementirajte operatorje sestevanja in odstevanja pri tem razredu, 
// in preverite da zares pravilno delujejo
//
// Naloga 3 : 
// Naredite razred vektor3D, ki ima operatorje + in -.
// Preverite, da implementirani operatorji zares delujejo.
// 
// Naloga 4 : 
// Naredite razred ulomek, ki ima operatorje +, -, * in /.
// V razredu implementirajte funkcijo, ki to stevilo primerno izpise.
// Preverite, da implementirani operatorji zares delujejo.

/*
 * Operator v porgramskem jeziku C++ 
 * je posebna funkcija razreda, ki izvrsi doloceno operacijo. 
 */

#include <iostream>

using namespace std;

class cas {
	public : 
		cas( int ure, int minute, int sekunde ) 
		{
			_ure = ure;
			_minute = minute;
			_sekunde = sekunde;
			cout << "Konstruiram cas : ";
			izpisi();
		}
		void izpisi() 
		{
			cout << "T = " << _ure << ":" << _minute << "." << _sekunde << endl;
		}
	private :
		int _ure;
		int _minute;
		int _sekunde;
	friend cas operator + (cas & c1, cas & c2 );
	friend cas operator - (cas & c1, cas & c2 );
};

cas operator + ( cas & c1, cas & c2 )
{
	int ure = 0, minute = 0, sekunde = 0;
	sekunde = c1._sekunde + c2._sekunde;
	if( sekunde > 60 ) 
	{
		minute += 1;
		sekunde -= 60;
	}

	minute += c1._minute + c2._minute;
	if (minute > 60 )
	{
		ure += 1;
		minute -= 60;
	}

	ure += c1._ure + c2._ure;
	return cas( ure, minute, sekunde );
}

cas operator - ( cas & c1, cas & c2 )
{
	int ure = 0, minute = 0, sekunde = 0;
	sekunde = c1._sekunde - c2._sekunde;
	if ( sekunde < 0 ) 
	{
		minute -= 1;
		sekunde += 60;
	}
	cout << "Prestopne minute : " << minute << ", sekunde = " << sekunde << endl;

	minute += c1._minute - c2._minute;
	if (minute < 0 )
	{
		ure -= 1;
		minute += 60;
	}
	ure += minute / 60;
	minute = (minute + 60) % 60;

	ure += c1._ure - c2._ure;
	return cas( ure, minute, sekunde );
}

int main( void ) 
{
	cas c1( 12, 45, 00 );
	c1.izpisi();
	cas c2( 1, 50, 22 );
	c2.izpisi();
	cas c3 = c1 + c2;
	cas c4 = c1 - c2;
	c3.izpisi();
	c4.izpisi();
	return 0;
}
