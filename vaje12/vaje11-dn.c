// DOMACA NALOGA : 
// Naloga 6 # : 
// Napisite funkcijo, ki kot argument sprejme kazalec na celo stevilo, in ga poveca za 5.
void povecaj_za_pet( int * stevilo ) 
{
	*stevilo += 5;
}

// Naloga 7 # : 
// Napisite funkcijo, ki kot argumenta sprejme niz, dolzino niza in dva znaka (A in B), 
// in v nizu vse znake A zamenja z znaki B.
// "Marko skace po zeleni" A="a" B="#" —> "M#rko sk#ce po zeleni tr#vi"
void zamenjaj_znak( char * niz, int dolzina, char A, char B )
{
	int i;
	for( i=0; i<dolzina; ++i ) 
	{
		if( niz[i] == A )
			niz[i] = B;
	}
}
// Naloga 12 # : 
// Napisite funkcijo, ki obrne podan niz. 
// Npr : "Marko skace" -> "ecaks okraM"
void obrni_niz( char * niz, int dolzina ) 
{
	char c;
	int i,j;
	i=0; j=dolzina-1;
	while( i < j ) 
	{
		c = niz[j];
		niz[j] = niz[i];
		niz[i] = c;
		i += 1;
		j -= 1;
	}
}

int main(void) 
{
	int n = 7;
	printf("Povecujem za 5 :\n");
	printf("Prej : n = %d\n", n );
	povecaj_za_pet(&n);
	printf("Po tem : n = %d\n", n );


	char niz[] = "Marko skace po zeleni";
	char A = 'a';
	char B = '#';
	printf("Zamenjujem znak '%c' z znakom '%c' v nizu '%s'\n", A, B, niz );
	zamenjaj_znak( niz, strlen(niz), A, B );
	printf("Zamenjan niz : '%s'\n", niz );

	char niz2[] = "Marko skace"; 
	printf("Obracam niz :\n");
	printf("Prej     : '%s'\n", niz2 );
	obrni_niz( niz2, strlen(niz2) );
	printf("Po tem   : '%s'\n", niz2 );
	return 0;
}
