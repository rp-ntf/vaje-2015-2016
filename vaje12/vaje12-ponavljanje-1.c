/*
 * Vaje 11 : 
 * ponavljanje : 
 * - strukture
 * - zanke 
 * - kazalci 
 * - nizi 
 * - funkcije 
 * - pisanje v datoteko 
 */

// Naloga 8 : 
// Napisite funkcijo, ki kot argument sprejme kazalec na celo stevilo, in ga razpolovi
//
// Naloga 9 : 
// Napisite funkcijo, ki kot argument sprejme kazalec na celo stevilo, in ga podvoji
//
// Naloga 10 : 
// Napisite funkcijo, ki kot argument sprejme celo stevilo, tabelo celih stevil in najvecjo dolzino te tabele. 
// Funkcija napolni tabelo z deljitelji danega celega stevila
// Npr : 24 -> [ 1, 2, 3, 4, 6, 8, 12, 24 ]
//
// Naloga 11 : 
// Napisite funkcijo, ki kot argument sprejme celo stevilo, tabelo celih stevil in najvecjo dolzino te tabele. 
// Funkcija napolni tabelo s prastevilskim razcepom danega stevila. 
// Npr : 24 -> [ 2, 2, 2, 3 ]
//
// Naloga 13 : 
// Napisite funkcijo, ki zdruzi dana dva niza, ki jih sprejme kot argumenta
// Npr : "Marko" in "Skace" -> "MarkoSkace"
//
// Naloga 14 : 
// Napisite funkcijo, ki pobrise presledke iz podanega niza.
// Npr : "Marko skace po zeleni travi" -> "Markoskacepozelenitravi" 
//
// Naloga 15 # : 
// Napisite funkcijo, ki zapise podano tabelo celih stevil v datoteko, po vrsticah.
//
// Naloga 16 # : 
// Napisite funkcijo, ki iz datoteke prebere tabelo celih stevil 
//
// Naloga 17 : 
// Napisite funkcijo, ki v datoteko zapise crke iz niza, po eno cko v vsako vrstico. 
//
// Naloga 18 : 
// Napisite funkcijo, ki iz datoteke prebere niz, ki je bil shranjen z eno crko v vrstico.
//
// Naloga 19 : 
// Napisite funkcijo, ki prebere datoteko s stevili (dve stevili v vrstico), iz izpise vsote stevil v vrstici skupaj s prvotnimi stevili v novo datoteko 
// Primer vhodne datoteke : 
// 1.0 2.1
// 7.1 2.4
// Primer izhodne datoteke : 
// 1.0 + 2.1 = 3.1
// 7.1 + 2.4 = 9.5
int main(void)
{
	return 0;
} 
