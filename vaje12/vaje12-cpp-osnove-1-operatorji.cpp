/*
 * Vaje 12 : C++, operatorji 
 */
#include <iostream>

/*
 * Operator v porgramskem jeziku C++ 
 * je posebna funkcija razreda, ki izvrsi doloceno operacijo. 
 */
using namespace std;

class vektor 
{
	public : 
		vektor( float x, float y )
			:_x(x), _y(y)
		{}
		// Operator ++ : 
		// definira kaj se zgodi ko objekt razreda vektor povecamo za 1 z ++ operatorjem
		void operator ++ (int) 
		{
			cout << "# Operator ++ na vektorju :\n# Ta vektor = ";
			izpisi();
			_x += 1.5;
			_y += 3.5;
		}
		// Operator ++
		// definira kaj naredi operacija sestevanja "+" na objektih tipa vektor.
		// Izraz v3 = v1 + v2 se izvrsi tako : 
		// Na objektu v1 se izvrsi operator +, ki kot argument operatorja 
		// dobi vektor v2.
		// Ta operator vrne zacasno spremenljivko tipa vektor, 
		// ki se potem skopira v pravi vektor v3
		//
		// Obicajno binarne operatorje ( = operatorje, ki delujejo na dveh instancah razreda ) 
		// definiramo zunaj razreda (glej operator - spodaj)
		vektor operator + ( vektor & v )
		{
			cout << "# Operator + na vektorjih : \n# v = ";
			v.izpisi();
			cout << "# Ta vektor = ";
			izpisi();
			return vektor( _x + v._x, _y + v._y );
		}
		void izpisi() const
		{
			std::cout << "Vektor = ( " << _x << ", " << _y << " )" << std::endl;
		}
	private : 
		float _x;
		float _y;

		friend vektor operator-( vektor const & v1, vektor const & v2 );
};

// Operator odstevanja : definira kaj se zgodi, ko odstejemo en vektor od drugega
vektor operator-( vektor const & v1, vektor const & v2 ) 
{
	cout << "# Operator - na vektorjih : \n##  v1 = ";
	v1.izpisi();
	cout << "# v2 = "; 
	v2.izpisi();
	return vektor( v1._x - v2._x, v1._y - v2._y );
}

int main( void ) 
{
	vektor v1(10,3), v2(3, 2), v3( 0, 0);
	v1.izpisi();
	v2.izpisi();
	v3.izpisi();
	std::cout << "Nastavim v3 = v1 + v2" << std::endl;
	v3 = v1 + v2;
	v3.izpisi();
	std::cout << "Nastavim v3 = v1 - v2" << std::endl;
	v3 = v1 - v2;
	v3.izpisi();
	std::cout << "Izvrsim v3++" << std::endl;
	v3++;
	v3.izpisi();
	return 0; 
}
