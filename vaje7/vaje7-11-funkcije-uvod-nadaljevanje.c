#include <stdio.h>

int povecaj_za_ena( int x );
// Funkcija poveca za 1
int povecaj_za_ena( int x )
{
	int y; 
	y = x+1;
	return y;
}

// Dekleriramo funkcijo : 
int zmanjsaj_za_ena( int x );

// Naloge : 
// - vsota dveh celih stevil
// - kvocient dveh celih stevil 
// - kvocient dveh celih stevil, izracunan kot realno stevilo 
//
// Domaca naloga 1 : OBVEZNA
// sestavi funkcijo, ki kot parameter prejme stevilo zvezdic, ki jih izpise na ekrat.
// f(4) izpise
// ****
//
// Domaca naloga 2 : 
// Naredi funkcijo, ki izracuna, kako dolgo bo trajalo preden kamen pade na tla. 
// Kamen vrzemo  v zrak, navpicno navzgor s hitrostjo v0 (ki je parameter funkcije)
//
// Domaca naloga 3 : 
// Naredite funkcijo, ki vrne kako dalec prileti kamen, ki ga s hitrostjo v0 
// vrzemo pod kotom 45 stopinj 
//
// Privzamemo : 
// g = 10 (tezni pospesek)
// Zracnega upora ni.

int main(void)
{
	printf("Povecaj za ena :\n");
		
	printf("%d -> %d\n", 4, povecaj_za_ena( 4 ) );
	printf("%d -> %d\n", 4, povecaj_za_ena( povecaj_za_ena( 4 ) ) );

	printf("Zmanjsaj za ena : \n");
	printf("%d -> %d\n", 4, zmanjsaj_za_ena( 4 ) );

	return 0;
} 

// zmanjsaj za 1
int zmanjsaj_za_ena( int x )
{
	return x-1;
}
