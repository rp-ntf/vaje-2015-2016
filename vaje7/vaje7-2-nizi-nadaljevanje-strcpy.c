/*
 * Nizi : nadaljevanje
 * Naloga : 
 */

#include <stdio.h>
#include <string.h>

int main(void)
{
	int MAX = 256;
	int i; // stevec
	char * p; // kazalec, več o tem naslednjič

	// STRCPY
	// #include <string.h>
	// char * strcpy( char cilj[], char izvor[] )
	//
	// Funkcija strcpy skopira niz izvor v niz cilj.
	char cilj[MAX];
	char izvor[MAX];
	printf("Vnesite niz [izvor] : ");
	gets(izvor);
	printf("Vnesite niz [cilj]  : ");
	gets(cilj);
	printf("Niz [izvor]                 : \"%s\".\n", izvor);
	printf("Niz [cilj, pred kopiranjem] : \"%s\".\n", cilj);
	strcpy( cilj, izvor );
	printf("Niz [cilj, po kopiranju]    : \"%s\".\n", cilj);

	// Funkcijo strcpy lahko uporabimo, da naredimo tabelo nizov
	// Ker je niz sam po sebi tabela znakov, 
	// moramo za tabelo nizov uporabiti 2D tabelo :
	char tabelaNizov[4][MAX];
	// kjer je prvi indeks stevilo vrstic, drugi pa stevilo stolpcev. 
	// Prvi indeks pove stevilo nizov v tabeli, 
	// drugi indeks pa pove najvecje dovoljeno stevilo znakov v kateremkoli nizu
   	strcpy( tabelaNizov[0], "nic" );
   	strcpy( tabelaNizov[1], "ena" );
   	strcpy( tabelaNizov[2], "dva" );
   	strcpy( tabelaNizov[3], "tri" );

	// Nize v tabeli lahko izpisemo na obicajen nacin, s tem da se moramo zavedati, 
	// da je tabelaNizov[i] zdaj niz :
	for( i=0; i<4; ++i )
	{
		printf("tabelaNizov[%d] = \"%s\"\n", i, tabelaNizov[i] );
	}

	return 0; 
}

