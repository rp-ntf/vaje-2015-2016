/*
 * Tabele : 2D 
 * Naloga : 
 * Sestavite program, 
 * ki od uporabnika prebere 1D vektor dolzine m=3 v[m], 
 * in 2D matriko mat[n][m] dimenzije 3x3 (nxm, m=3, n=3), 
 * ter vrne  produkt matrike in vektorja w[m]
 *
 * w = mat . v
 *
 * definiran kot 
 * w[i] = sum_j=0...m mat[i][j] * v[j]
 *
*/

#include <stdio.h>

int main(void)
{
	int n=3;
	int m=3;
	int v[m], w[n], tab[n][m];
	int i,j;
	// Najprej preberemo vektor :
	for(i=0; i<m; ++i )
	{
		printf("v[%d] = ",i);
		scanf("%d", &v[i]);
	}
	// izpisemo vektor : 
	for( i=0; i<m; ++i )
	{
		printf("v[%d] = %d\n", i, v[i] );
	}
	// potem preberemo matriko 
	for(i=0; i<n; ++i )
	{
		for( j=0; j<m; ++j )
		{
			printf("tab[%d][%d] = ",i,j);
			scanf("%d", &tab[i][j]);
		}
	}
	// Izpisemo matriko : 
	for(i=0; i<n; ++i )
	{
		for( j=0; j<m; ++j )
		{
			printf("tab[%d][%d] = %d\n",i,j,tab[i][j]);
		}
	}
	// izracunamo produkt 
	int vsota;
	for( i=0; i<m; ++i )
	{
		// Izracunajmo w[i]
		vsota = 0;
		for( j=0; j<n; ++j )
		{
			vsota += tab[i][j] * v[j];
		}
		w[i] = vsota;
	}
	
	// izpisemo koncen vektor 
	for( i=0; i<m; ++i )
	{
		printf("w[%d] = %d\n", i, w[i] );
	}

	return 0;  
}

