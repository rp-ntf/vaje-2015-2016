/*
 * Domaca naloga : 
 * sestavite funkcijo izpisi tabelo 
 * za tabele tipov char, int, float, double
 */

#include <stdio.h>

// char 
void izpisi_tabelo_char( char * tab, int n )
{
	int i=0; 
	printf("Tabela [%2d] :\n", n);
	for( i=0; i<n; ++i )
	{
		printf("%2d : %c\n", i, tab[i]);
	}
}

// int 
void izpisi_tabelo_int( int * tab, int n )
{
	int i=0; 
	printf("Tabela [%2d] :\n", n);
	for( i=0; i<n; ++i )
	{
		printf("%2d : %d\n", i, tab[i]);
	}
}

// float 
void izpisi_tabelo_float( float * tab, int n )
{
	int i=0; 
	printf("Tabela [%2d] :\n", n);
	for( i=0; i<n; ++i )
	{
		printf("%2d : %f\n", i, tab[i]);
	}
}

// double 
void izpisi_tabelo_double( double * tab, int n )
{
	int i=0; 
	printf("Tabela [%2d] :\n", n);
	for( i=0; i<n; ++i )
	{
		printf("%2d : %lf\n", i, tab[i]); 
	}
}

int main( void ) 
{
	char tab_char[5] = { 60, 61, 62, 63, 64 };
	int tab_int[4] = { 1, 2, 3, 4 };
	float tab_float[3] = { 2.345, 3.456, 4.567 };
	double tab_double[2] = { 1.23456, 7.8901234 };

	izpisi_tabelo_char( tab_char, 5 );
	izpisi_tabelo_int( tab_int, 4 );
	izpisi_tabelo_float( tab_float, 3 );
	izpisi_tabelo_double( tab_double, 2 ); 
	return 0;
}
