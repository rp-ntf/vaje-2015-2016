/*
 * Delo z datotekami : uvod 
 */

#include <stdio.h>

int main( void ) 
{
	// Datoteko v programskem jeziku C predstavimo s kazalcem na spremenljivko tipa FILE
	FILE * f_pisi;
	// Da lahko z datoteko delamo (vanjo zapisujemo podatke, iz nje beremo, ... ), jo moramo najprej odpreti
	f_pisi = fopen("test1.txt", "wt");
	// funkcija "fopen" odpre datoteko. Njeni argumenti so : 
	//
	// #include <stdio.h>
	// FILE *fopen(const char *path, const char *mode);
	// "path" je ime datoteke. Primeri so 
	// "mode" pa je nacin odpiranja datoteke, in je sestavljen iz kombinacije znakov z razlicnimi pomeni :
	//
	//		r : odpri za branje
	//		w : odpri za pisanje 
	//		a : odpri za dodajanje (vsako pisanje doda vsebino na konec datoteke)
	//
	//		b : odpri kot podatkovno datoteko 
	//		t : odpri kot tekstovno datoteko 
	//
	//	Primeri : 
	//	f = fopen("test_pisi.txt", "wt");
	//	odpre datoteko "test_pisi.txt" za pisanje kot tekstovno datoteko 
	//
	//	f = fopen("test_beri.dat", "rb"); 
	//	odpere datoteko "test_beri.dat" za branje kot podatkovno datoteko 
	//
	//	f = fopen("test_dodaj.txt", "at");
	//	odpre datoteko "test_dodaj.txt" za dodajanje. Dodajanje pomeni, da bo vsak zapis v datoteko dodan na konec datoteke.
	//
	//	Ce datoteke ni mogoce odpreti, funkcija vrne vrednost NULL
	if( f_pisi == NULL ) 
	{
		printf("Datoteke ni bilo mogoce odpreti\n");
		return 1;
	}

	// Za izpis v datoteko in branje iz datoteke uporabljamo podobne funkcije kot za izpis na zaslon in branje od uporabnika. 
	// Funkcije za delo z datoteko dobijo pred ime crko "f", njihov prvi / zadnji argument pa postane kazalec na datoteko : 
	//
	// pisanje :
	// printf(...) -> fprintf(f, ...)
	// branje :
	// scanf(...)  -> fscanf(f, ...)
	//
	// branje vrstice :
	// gets( niz )   -> fgets( niz, MAX, f)
	//
	// branje znaka :
	// getc()   -> fgetc(f)
	//
	// Pri branju iz datoteke se lahko zgodi, da preberemo vse do konca datoteke. 
	// Tak dogodek funkcije za branje sporocijo na razlicne nacine : 
	// - fscanf v primeru branja cez konec datoteke vrne vrednost EOF, sicer vrne stevilo uspesno prebranih podatkov
	// - fgets v primeru branja cez konec datoteke vrne vrednost NULL, sicer vrne kazalec na prebran niz
	// - fgetc v primeru branja cez konec datoteke vrne vrednost EOF, sicer vrne prebran znak 
	//
	// Podrobneje : 
	// najprej zapisemo v datoteko pozdrav s funkcijo 
	fprintf(f_pisi, "Pozdravljen svet!\n" );
	fprintf(f_pisi, "Ze spet pozdravljen svet!\n" );
	// po koncanju zapisa datoteko zapremo z 
	fclose(f_pisi);
	
	// Zdaj pa zelimo zapisano prebrati nazaj in izpisati na zaslon. 
	// Najprej odpremo datoteko za branje : 
	FILE * f_beri;
	f_beri = fopen("test1.txt", "rt");
	int MAX = 256;
	char vrstica[MAX];
	while( fgets( vrstica, MAX-1, f_beri ) != NULL )
	{
		printf("%s", vrstica); // POZOR ! tu ni znaka za prehod v novo vrstico, na zaslon pa se kljub temu izpise vsaka vrstica v novo vrstico. To je zaradi znaka \n v datoteki.
	}
	// in po koncanju branja datoteko tudi zapremo 
	fclose( f_beri );

	return 0;
}
