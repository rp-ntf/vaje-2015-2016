/* 
 * Funkcije, kazalci : ponavljanje 
 */

#include <stdio.h>
#include <math.h>

// int 
void izpisi_tabelo_int( int * tab, int n )
{
	int i=0; 
	printf("Tabela [%2d] :\n", n);
	for( i=0; i<n; ++i )
	{
		printf("%2d : %d\n", i, tab[i]);
	}
}

//  Naloga : 
//  napisite funkcijo, ki 
//  - izracuna razdaljo med tockami v 2D #
float razdalja_2D( float * v1, float * v2 ) 
{
	float vsota = 0;
	vsota += (v1[0]-v2[0]) * (v1[0]-v2[0]);
	vsota += (v1[1]-v2[1]) * (v1[1]-v2[1]);
	return sqrt(vsota);
}

//  - izracuna razdaljo med tockami v 3D 
float razdalja_3D( float * v1, float * v2 ) 
{
	float vsota = 0;
	vsota += (v1[0]-v2[0]) * (v1[0]-v2[0]);
	vsota += (v1[1]-v2[1]) * (v1[1]-v2[1]);
	// DODAMO : 
	vsota += (v1[2]-v2[2]) * (v1[2]-v2[2]);

	return sqrt(vsota);
}
//  - izracuna razdaljo med tockami v N dimenzijah (N=1, 2, 3, .. )
float razdalja_ND( float * v1, float * v2, int n ) 
{
	float vsota = 0;
	int i;
	for( i=0; i<n; ++i )
		vsota += (v1[i]-v2[i]) * (v1[i]-v2[i]);
	return sqrt(vsota);
}
//
//  - izracuna e^x in e^-x dane vrednosti x in obe vrednosti vrne preko kazalcev #
void exp_in_minus_exp( float x, float * e_x, float * e_mx )
{
	*e_x = exp(x);
	*e_mx = exp( 0 - x );
}

//  - izracuna sinus, kosinus in tangens podanega kota in vse tri vrednosti vrne preko uporabe kazalcev 

//  Naloga : 
//  definirajte funkcijo Cosh( x )  - cosinus hiperbolicus 
//  in izpisite njene vrednosti na intervalu od -1 do 1 v korakih po 0.2
//  Cosh( x ) = ( exp(x) + exp(-x) ) / 2
//  Izpis : 
//  -1 : 1.5430....
//  -0.8 : 1.33743...
//  ...
//  0.8 : 1.33743...
//  1 : 1.5430....
float Cosh( float x )
{
	// moznost 1 : 
	return ( exp(x) + exp( -x) ) / 2.0;
	// // moznost 2 :
	// float e_x, e_mx;
	// exp_in_minus_exp( x, &e_x, &e_mx);
	// return (e_x + e_mx) / 2.0;
}

//  - izracuna vsoto vektorjev po komponentah  #
//  - izracuna razliko vektorjev po komponentah 
//  - izracuna produkt vektorjev po komponentah 
int main( void ) 
{
	float x, e_x, e_mx;
	for( x=-1.0; x <1.0 + 0.01; x += 0.2 )
	{
		// Ce je x > 0, izpisi ' ' 
		if( x > 0 ) 
			printf(" ");
		printf("%f : %f\n", x, Cosh(x) );
	}



	x = 1.0;
	exp_in_minus_exp( x, & e_x, & e_mx );
	printf("x = %f, exp(x) = %f, exp(-x) = %f\n", x, e_x, e_mx );
	x = -2;
	exp_in_minus_exp( x, & e_x, & e_mx );
	printf("x = %f, exp(x) = %f, exp(-x) = %f\n", x, e_x, e_mx );

	float v1[2] = { 3.0, 0.0 };
	float v2[2] = { 0.0, 4.0 };
	printf("Razdalja 2D je : %f\n", razdalja_2D( v1, v2 ) );
	printf("Razdalja ND je : %f\n", razdalja_ND( v1, v2, 2 ) );

	float v1_3D[3] = { 3.0, 2.4, 5.2 };
	float v2_3D[3] = { 1.7, 3.6, 8.1 };
	printf("Razdalja 3D je : %f\n", razdalja_3D( v1_3D, v2_3D ) );
	printf("Razdalja ND je : %f\n", razdalja_ND( v1_3D, v2_3D, 3 ) );

	return 0;
}

//
//  - izracuna skalarni produkt dveh vektorjev
//		a . b = sqrt( sum a[i] * b[i] )
//
//  - izpise zadnjo besedo v danem nizu 

