/*
 * Delo z datotekami : osnove 
 */

#include <stdio.h>
#include <math.h>
/* 
 * Naloga : #
 * sestavite program, ki v datoteko po stoplcih izpise vrednosti x in funkcije sin(x)
 * na intervalu -1, 1 v korakih po 0.2
 */
int main(void) 
{
	float x; 
	FILE * f;
	f = fopen("test2.txt", "wt" );
	if ( f == NULL ) 
	{
		printf("Tezave z odpiranjem datoteke\n");
		return 1;
	}
	for( x=-M_PI; x<M_PI + 0 + 0.1; x += 0.2 )
	{
		// ce izpisujemo na zaslon : 
		// printf("%f ; %f\n", x, sin(x) );
		// v datoteko : 
		fprintf(f, "%f ; %f\n", x, sin(x) );
	}
	return 0;
}
