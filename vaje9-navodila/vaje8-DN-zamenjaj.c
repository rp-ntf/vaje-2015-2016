/* 
 * Domaca naloga : 
 * sestavite funkcijo zamenjaj elementa, 
 * za vsakega izmed tipov elementov 
 * char, int, float, double 
 */

#include <stdio.h>

// char 
void zamenjaj_char( char * p1, char * p2 )
{
	char x;
	x = *p1; 
	*p1 = *p2;
	*p2 = x;
}

// int 
void zamenjaj_int( int * p1, int * p2 )
{
	int x;
	x = *p1; 
	*p1 = *p2;
	*p2 = x;
}

// float 
void zamenjaj_float( float * p1, float * p2 )
{
	float x;
	x = *p1; 
	*p1 = *p2;
	*p2 = x;
}

// double 
void zamenjaj_double( double * p1, double * p2 )
{
	double x;
	x = *p1; 
	*p1 = *p2;
	*p2 = x;
}

int main(void) 
{
	// char :
	char a_char, b_char;
	a_char = 85;
	b_char = 78;
	printf("Pred zamenjavo : a = %c, b = %c\n", a_char, b_char);
	zamenjaj_char( & a_char, & b_char );
	printf("Po zamenjavi   : a = %c, b = %c\n", a_char, b_char);
	
	// int :
	int a_int, b_int;
	a_int = 1;
	b_int = 2;
	printf("Pred zamenjavo : a = %d, b = %d\n", a_int, b_int);
	zamenjaj_int( & a_int, & b_int );
	printf("Po zamenjavi   : a = %d, b = %d\n", a_int, b_int);
	
	// float :
	float a_float, b_float;
	a_float = 3.4;
	b_float = 5.6;
	printf("Pred zamenjavo : a = %f, b = %f\n", a_float, b_float);
	zamenjaj_float( & a_float, & b_float );
	printf("Po zamenjavi   : a = %f, b = %f\n", a_float, b_float);
	
	// double :
	double a_double, b_double;
	a_double = 7.890;
	b_double = 1.234;
	printf("Pred zamenjavo : a = %lf, b = %lf\n", a_double, b_double); 
	zamenjaj_double( & a_double, & b_double );
	printf("Po zamenjavi   : a = %lf, b = %lf\n", a_double, b_double);
	return 0;
}
