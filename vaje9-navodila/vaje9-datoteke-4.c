/*
 * Delo z datotekami : osnove 
 */

#include <stdio.h>

/* 
 * Naloga : #
 * sestavite program, ki v datoteko zapise tabelo celih stevil velikosti n (n=5)
 * potem pa to tabelo prebere iz datoteke in jo izpise na zaslon.
 *
 * Stevila v tabeli so lahko poljubna, ni jih potrebno brati od uporabnika.
 */

int main(void) 
{
	int tab[5] = { 2, 4, 6, 8, 10 };
	int n = 5;
	int i;
	FILE * f;
	f = fopen("test4.txt", "wt");
	for( i=0; i<n; ++i )
	{
		fprintf(f, "%d\n", tab[i] );
	}
	fclose(f);

	FILE * f_beri;
	f_beri = fopen("test4.txt", "rt");
	if( f_beri == NULL ) 
	{
		printf("Tezava pri odpiranju datoteke za branje \n");
		return 1;
	}
	int tab_beri[5];
	int je_EOF = 0;
	for( i=0; i<n; ++i )
	{
		je_EOF = fscanf( f_beri, "%d\n", &tab_beri[i] ); 
		if ( je_EOF == EOF )
		{
			printf("Datoteko smo prebrali do konca\n");
			break;
		}
	}
	for( i=0; i<n; ++i ) 
	{
		printf("Brano : %d\n", tab_beri[i] );
	}
	fclose(f_beri);
	return 0;
}

