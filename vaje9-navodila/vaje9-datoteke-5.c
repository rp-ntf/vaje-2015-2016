/*
 * Delo z datotekami : osnove 
 */

#include <stdio.h>

/* 
 * Naloga : 
 * sestavite program, ki v datoteko zapise tabelo realnih stevil (float) velikosti n, 
 * potem pa to tabelo prebere iz datoteke in jo izpise na zaslon.
 *
 * Stevila v tabeli so lahko poljubna, ni jih potrebno brati od uporabnika.
 */

int main(void) 
{
	float tab[4] = {
		1.23,
		2.34, 
		3.45, 
		4.56,
	};
	int n = 4;
	int i;
	FILE * f;
	f = fopen("test5.txt", "wt");
	for( i=0; i<n; ++i )
	{
		fprintf(f, "%f\n", tab[i] );
	}
	fclose(f);

	FILE * f_beri;
	f_beri = fopen("test5.txt", "rt");
	if( f_beri == NULL ) 
	{
		printf("Tezava pri odpiranju datoteke za branje \n");
		return 1;
	}
	float tab_beri[4];
	int je_EOF = 0;
	for( i=0; i<n; ++i )
	{
		je_EOF = fscanf( f_beri, "%f\n", &tab_beri[i] );  
		if ( je_EOF == EOF )
		{
			printf("Datoteko smo prebrali do konca\n");
			break;
		}
	}
	for( i=0; i<n; ++i ) 
		printf("Brano : %f\n", tab_beri[i] );
	fclose(f_beri);
	return 0;
}
