/*
 * Delo z datotekami : osnove 
 */

#include <stdio.h>
#include <math.h>

float Cosh( float x )
{
	// moznost 1 : 
	return ( exp(x) + exp( -x) ) / 2.0;
	// // moznost 2 :
	// float e_x, e_mx;
	// exp_in_minus_exp( x, &e_x, &e_mx);
	// return (e_x + e_mx) / 2.0;
}
/* 
 * Naloga : 
 * sestavite program, ki v datoteko po stoplcih izpise vrednosti x  sin(x), cos(x), tan(x) in exp(x) 
 * ter se Cosh(x)
 */
int main(void) 
{
	float x; 
	FILE * f;
	f = fopen("test3.txt", "wt" );
	if ( f == NULL ) 
	{
		printf("Tezave z odpiranjem datoteke\n");
		return 1;
	}
	for( x=-M_PI; x<M_PI + 0 + 0.1; x += 0.2 )
	{
		// ce izpisujemo na zaslon : 
		// printf("%f ; %f\n", x, sin(x) );
		// v datoteko : 
		fprintf(f, "%f ; %f ; %f ; %f ; %f\n", x, sin(x), cos(x), tan(x), Cosh(x) );
	}
	return 0;
}
