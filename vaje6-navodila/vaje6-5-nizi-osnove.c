/*
 * Nizi : osnove
 * Naloga : 
 */

#include <stdio.h>

int main(void)
{
	int MAX = 256;
	int i; // stevec

	// Niz je tabela znakov (spremenljivk tipa char) : 
	// Lahko ga definiramo kot : 
	char niz1[] = { 'p', 'r', 'v', 'i', ' ', 'n', 'i', 'z', '\0' };
	// in izpisemo z formatnim določilom %s :
	printf("niz1 : %s.\n", niz1 );
	// kjer smo zadnji element tabele nastavili na vrednost '\0',
	// ki označuje konec niza. 
	// Vrednosti znaka '\0' (char) je 0 (int).
	//
	// Kot vemo, moramo znake (črke) podati v enojnih oklepajih ('x').
	//
	// Niz lahko definiramo tudi kot 
	char niz2[] = "drugi niz";
	printf("niz2 : %s.\n", niz2 );
	// (tekst v dvojnih oklepajih)
	// kjer prevajalnik sam poskrbi za dodajanje elementa '\0' na konec niza.
	
	// STRLEN
	// #include <string.h>
	// unsigned int strlen( char niz[] );
	//
	// Funkcija strlen pove dolžino niza, v znakih brez zadnjega znaka '\0'
	// printf("Dolzina niza niz2 je %d.\n", strlen(niz2) );
	printf("Dolzina niza niz1 je %ld.\n", strlen(niz1) );
	printf("Dolzina niza niz2 je %ld.\n", strlen(niz2) );

	// Elemente niza lahko dostopamo na enak način kot elemente tabele : 
	for( i=0; i<=strlen(niz1); ++i )
		printf("niz1[%d] = '%c' = %d\n", i, niz1[i], niz1[i] );
	// kjer se moramo zavedati, da je niz tabela dolzine strlen(niz) + 1.
	
	// Manipulacija z nizi 
	//
	// STRCHR
	// #include <string.h>
	// char * strchr( char niz[], int c );
	// 
	// Funkcija strchr najde znak v nizu. 
	// Vrne indeks kazalec na najden znak, oziroma NULL če ga ne najde. 
	// (NULL je konstanta, definirana kot NULL = 0)
	// Več o kazalcih kasneje. 
	// Indeks najdenega znaka lahko dobimo tako, da rezultatu odštejemo kazalec na začetek niza.

	if( strchr(niz1, 'n') != NULL ) 
		printf("Indeks znaka n je %ld\n", strchr(niz1, 'n') - niz1 );
	if( strchr(niz1, 'i') != NULL ) 
		printf("Indeks znaka i je %ld\n", strchr(niz1, 'i') - niz1 );
	if( strchr(niz1, 'z') != NULL ) 
		printf("Indeks znaka z je %ld\n", strchr(niz1, 'z') - niz1 );

	// STRCMP 
	// #include <string.h>
	// int strcmp( char niz1[], char niz2[] );
	//
	// Funkcija strcmp primerja dva niza.
	// V primeru, da sta niza enaka, vrne vrednost 0. 
	// V primeru, da je niz1 leksikografsko manjši od niz2 vrne negativno vrednost, 
	// sicer vrne pozitivno vrednost.
	//  vrne ~ (niz1 - niz2)
	char niz3[] = "nov niz";
	printf("strcmp( \"%s\", \"%s\" ) = %d\n", niz1, niz3, strcmp(niz1, niz3) );
	printf("strcmp( \"%s\", \"%s\" ) = %d\n", niz2, niz3, strcmp(niz2, niz3) );
	printf("strcmp( \"%s\", \"%s\" ) = %d\n", niz1, niz2, strcmp(niz1, niz2) );
	printf("strcmp( \"%s\", \"%s\" ) = %d\n", niz3, niz3, strcmp(niz3, niz3) );

	// IZPIS IN BRANJE 
	// Za izpis in branje nizov uporabljamo formatno dolocilo "%s". 
	char niz4[MAX];
	// Pri branju s pomocjo scanf moramo upostevati, 
	// da program prebere le prvo besedo v vrstici, 
	// in da pred nizom ne podemo znaka "&"
	printf("Vnesite niz : ");
	scanf("%s", niz4 );
	// in ne : ne scanf("%s", &niz4)
	printf("Prebrali smo (scanf) niz \"%s\".\n", niz4);
	
	// * Ker scanf v prejsnjem branju prebere le prvo besedo, 
	// moramo pred naslednjim vnosom pobrisati preostanek iz vhodnega pomnilnika.
	gets(niz4); // spodaj
	printf("V pomnilniku je ostalo \"%s\"\n", niz4 );
	// BRANJE CELE VRSTICE
	// S pomocjo funkcije gets lahko preberemo celo vrstico
	//
	// #include <stdio.h>
	// char * gets(char niz[] )
	printf("Vnesite niz : ");
	gets(niz4);
	printf("Prebrali smo (gets) niz \"%s\".\n", niz4);

	return 0; 
}

