/*
 * Tabele : ponavljanje 
 * Napiši program, ki uredi tabelo. 
 *
 * Urejanje naj bo narejeno na kar se da enostaven način : 
 * 1. na vsakem koraku algoritma naredimo : 
 *	1a : preverimo, ali je tabela urejena. Če je, prekinemo izvajanje 
 *	1b : gremo v zanki čez celo tabelo, in v primeru da 
 *		tab[i] > tab[i+1] 
 *		zamenjamo elementa i in i+1.
 *	Dodatna vprasanja / naloge : 
 *	- najvec kolikokrat moramo izvrsiti zunanjo zanko ? 
 */

#include <stdio.h>

int main(void)
{
	int i; // števec
	int tmp; // zacasna spremenljivka za zamenjavo vrednosti
	int n; // velikost tabele 
	int MAX = 256; // najvecja dovoljena velikost tabele
	// Deklaracija tabele 
	int tab[MAX];
	// uporabljena velikost tabele :
	printf("Vnesite stevilo elementov tabele : ");
	scanf("%d", &n);
	if( n > MAX ) // Tabela ne sme biti večja od MAX
		n = MAX;
	for( i=0; i<n; ++i )
	{
		printf("Vnesite %d-to stevilo tabele [%d] : ", i, n);
		scanf("%d", &tab[i] );
	}

	// Zunanja zanka
	// while( 1 )
	// {
	// 1.a : ali je tabela urejena ? Uporabimo enak algoritem kot v prejsnem programu (kopiraj sem)

	// 		// Preverimo, če je tabela urejena 
	// 		int jeUrejena = 1; // =1 če je urejena, =0 če ni
	// 		/* 
	// 		 * Algoritem : 
	// 		 * v zanki gremo čez vse elemente tabele, 
	// 		 * for( ... )
	// 		 * { 
	// 		 * in na vsakem koraku preverimo, ali je dani element res manjši od naslednjega elementa.
	// 		 *	if( tab[ ?? ] < tab[ ?? +1 ] )
	// 		 *	{
	// 		 *		Če to ni res, označimo tabelo kot ne-urejeno, in prekinemo izvajanje zanke. 
	// 		 *	}
	// 		 *
	// 		 */
	// if (jeUrejena == 1) 
	// {
	// 	printf("Tabela je urejena.\n");
	// 	break;
	// }
	//
	// 1.b : v zanki gremo cez celo tabelo 
	// for( i=0; i<n-1; ++i )
	// {
	// ce je tab[i] > tab[i+1], potem zamenjamo tab[i] in tab[i+1]
	// }

	return 0; 
}

