/*
 * Tabele : ponavljanje 
 * Napiši program, ki izračuna skalarni produkt dveh vektorjev, 
 * podanih kot tabeli poljubne dolžine.
 *
 */

#include <stdio.h>
#include <math.h>

int main(void)
{
	int i; // števec
	int n; // velikost tabele 
	int MAX = 256; // najvecja dovoljena velikost tabele
	// Deklaracija tabele 
	float a[MAX];
	float b[MAX];
	float sum = 0.0;
	// uporabljena velikost tabele :
	printf("Vnesite stevilo elementov tabele : ");
	scanf("%d", &n);
	if( n > MAX ) // Tabela ne sme biti večja od MAX
		n = MAX;
	for( i=0; i<n; ++i )
	{
		printf("a[%d] = ", i );
		scanf("%f", &a[i] );
	}
	for( i=0; i<n; ++i )
	{
		printf("b[%d] = ", i );
		scanf("%f", &b[i] );
	}

	// v zanki sestej vrednosti a[i]*b[i]
	// Skalarni produkt je koren izračunane vsote.

	return 0; 
}

