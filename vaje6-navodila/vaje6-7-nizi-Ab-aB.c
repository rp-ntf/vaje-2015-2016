/*
 * Nizi : osnove
 * Naloga : 
 * Sestavi program, ki v podanem nizu zamenja velike crke z malimi. 
 * Na primer, ce uporabnik vnese 
 * "To je PrIMer"
 * program izpise 
 * "tO JE pRimER"
 */

#include <stdio.h>

int main(void)
{
	int MAX = 256;
	char niz[MAX];
	// najprej preberemo niz od uporabnika (gets)
	// gets(...) 
	// Potem pa v zanki gremo skozi niz, in vsak majhen znak zamenjamo z velikim znakom.
	// for( i=0; i<??; ++i )
	// {
	// if( niz[i] je majhen znak )
	//	niz[i] = velikZnak(niz[i])
	// else if( niz[i] je velik znak )
	//	niz[i] = majhenZnak(niz[i])
	// }

	return 0; 
}

