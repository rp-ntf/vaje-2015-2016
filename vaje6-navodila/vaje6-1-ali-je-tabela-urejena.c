/*
 * Tabele : ponavljanje 
 * Napiši program, ki od uporabnika najprej prebere tabelo poljubne dolžine (n), 
 * in potem preveri, ali je tabela urejena po velikosti. 
 *
 * Tabela je urejena po velikosti, če velja : 
 * tab[0] <= tab[1] <= ... <= tab[n-1]
 *
 */

#include <stdio.h>

int main(void)
{
	int i; // števec
	int n; // velikost tabele 
	int MAX = 256; // najvecja dovoljena velikost tabele
	// Deklaracija tabele 
	int tab[MAX];
	// uporabljena velikost tabele :
	printf("Vnesite stevilo elementov tabele : ");
	scanf("%d", &n);
	if( n > MAX ) // Tabela ne sme biti večja od MAX
		n = MAX;
	for( i=0; i<n; ++i )
	{
		printf("Vnesite %d-to stevilo tabele [%d] : ", i, n);
		scanf("%d", &tab[i] );
	}


	// Preverimo, če je tabela urejena 
	int jeUrejena = 1; // =1 če je urejena, =0 če ni
	/* 
	 * Algoritem : 
	 * v zanki gremo čez vse elemente tabele, 
	 * for( ... )
	 * { 
	 * in na vsakem koraku preverimo, ali je dani element res manjši od naslednjega elementa.
	 *	if( tab[ ?? ] < tab[ ?? +1 ] )
	 *	{
	 *		Če to ni res, označimo tabelo kot ne-urejeno, in prekinemo izvajanje zanke. 
	 *	}
	 *
	 */
	if (jeUrejena == 1) 
		printf("Tabela je urejena.\n");
	else
		printf("Tabela ni urejena.\n");

	return 0; 
}

