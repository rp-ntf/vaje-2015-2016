/*
 * Nizi : osnove
 * Domaca naloga : 
 * Naredi program, ki se bo z uporabnikom igral vislice. 
 * Program ima v kodi definirano besedo (npr "domaca naloga"), 
 * ki jo uporabnik mora uganiti. 
 * Vmes program izpisuje stevilo porabljenih poskusov, in do zdaj uganjeno besedo. 
 * Npr : 
 * > ______ ______, 0 poskusov
 * > Vnesite crko : 
 * < a
 * > ___a_a _a___a , 0 poskusov
 * > Vnesite crko : 
 * < t
 * > Crke t ni v besedi
 * > _____a _a___a , 1 poskusov
 * > Vnesite crko : 
 * < o
 * > _o_a_a _a_o_a , 1 poskusov
 * ...
 *
 */

#include <stdio.h>

int main(void)
{
	return 0; 
}

