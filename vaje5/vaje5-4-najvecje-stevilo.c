/*
 * Tabele : 
 *
 * Naloga : (Drugo najvecje stevilo)
 * Sestavi program, ki   
 *	1. uporabnika najprej vpraša za največjo dolžino tabele (n)
 *	2. uporabnika vpraša za cela števila, ki jih shrani v to tabelo (tab)
 * 	3. Izpise najvecje stevilo v tabeli
 *
 * Dodatna naloga :
 * 	4. Izpise drugo najvecje stevilo v tabeli
 *
 */

#include <stdio.h>

int main(void)
{
	int i; // števec
	int n; // velikost tabele 
	int max1; // majvecje stevilo v tabeli 
	int max2; // drugo najvecje stevilo v tabeli 

	int MAX = 256; // najvecja dovoljena velikost tabele
	// Deklaracija tabele 
	int tab[MAX];
	// uporabljena velikost tabele :
	printf("Vnesite stevilo elementov tabele : ");
	scanf("%d", &n);
	if( n > MAX ) // Tabela ne sme biti večja od MAX
		n = MAX;
	if( n < 2 )
		n = 2;

	for( i=0; i<n; ++i )
	{
		printf("Vnesite %d-to stevilo tabele [%d] : ", i, n);
		scanf("%d", &tab[i] );
	}

	max1 = tab[0];
	// Največje število v tabeli : 
	for( i=0; i<n; ++i )
	{
		// Ali je i-ti element tabele večji od max1 ?
		if( tab[i] > max1 )
		{
			// Potem je do zdaj največje število tab[i]
			max1 = tab[i];
		}
	}
	printf("Najvecje stevilo v tabeli je %d.\n", max1);
	
	// Se drugo najvecje stevilo 
	if( tab[0] > tab[1] )
	{
		max1 = tab[0];
		max2 = tab[1];
	}
	else 
	{
		max2 = tab[0];
		max1 = tab[1];
	}
	for( i=2; i<n; ++i )
	{
		if( tab[i] > max1 )
		{
			max2 = max1;
			max1 = tab[i];
		}
		else if( tab[i] > max2 )
		{
			max2 = tab[i];
		}
	}
 
	printf("Drugo najvecje stevilo v tabeli je %d\n", max2 );

	return 0; 
}

