/*
 * Tabele : osnove
 */

#include <stdio.h>

int main(void)
{
	int i; // števec
	// Deklaracija 
	int tab1[6]; // Tabela 6 celih števil
	float tab2[7]; // Tabela 7 realnih števil
	
	// Deklaracija s prireditvijo vrednosti 
	int tab3[] = { 1, 2, 3, 4, 5 }; // Tabela 5 celih stevil 1..5
	float tab4[] = { 0.5, 1.0, 1.5, 2.0, 2.5 }; // Tabela 5 realnih stevil 

	// Vecdimenzionalne tabele :
	float tab2d[3][2];

	// Izpis tabele 
	printf("Izpisujemo tabelo : \n");
	for( i=0; i<5; ++i ) // Tabele se zacnejo z indeksom [0], zadnji element pa ima indeks [n-1]
	{
		printf("%d : %d \n", i, tab3[i]);
	}
	printf("\n");

	// Prirejanje vrednosti 
	tab3[2] = 99;
	// Izpis tabele( se enkrat)
	printf("Izpisujemo tabelo po prirejanju vrednosti : \n");
	for( i=0; i<5; ++i )
	{
		printf("%d : %d \n", i, tab3[i]);
	}
	printf("\n");

	// Vsota elementov tabele 
	int vsota = 0;
	for( i=0; i<5; ++i )
	{
		vsota += tab3[i];
	}
	printf("Vsota elementov tabele je %d\n", vsota );

	return 0;
}

