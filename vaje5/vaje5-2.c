/*
 * Tabele 
 *
 * Naloga :
 * Sestavi program, ki uporabnika najprej vpraša za velikost tabele (n), 
 * nato pa ga vpraša za vnost n celih števil, ki jih shrani v tabelo (tab).
 *
 * Program naj na koncu izpiše tabelo v obeh smereh.
 *
 * Dodatni nalogi : 
 *  - program naj izpise se vsako drugo stevilo 
 *  - program naj izpise le soda stevila
 */

#include <stdio.h>

int main(void)
{
	int i; // števec
	int n; // velikost tabele 
	// int MAX = 256; // najvecja dovoljena velikost tabele
	#define MAX 256
	// Deklaracija tabele 
	int tab[MAX];
	// uporabljena velikost tabele :
	printf("Vnesite stevilo elementov tabele : ");
	scanf("%d", &n);
	if( n > MAX ) // Tabela ne sme biti večja od MAX
		n = MAX;
	for( i=0; i<n; ++i )
	{
		printf("Vnesite %d-to stevilo tabele [%d] : ", i, n);
		scanf("%d", &tab[i] );
	}

	// Izpis naprej 
	printf("Izpisujemo tabelo naprej : \n");
	for( i=0; i<n; ++i )
		printf("%d ", tab[i]);
	printf("\n");

	// Izpis nazaj 
	printf("Izpisujemo tabelo nazaj : \n");
	for( i=n-1; i>=0; i-- )
		printf("%d ", tab[i]);
	printf("\n");

	// Izpis naprej (vsako drugo)
	printf("Izpisujemo tabelo naprej (vsako drugo) : \n");
	for( i=0; i<n; ++i )
	{
		if( i%2 == 0 )
			printf("%d ", tab[i]);
	}
	printf("\n");
	
	// Izpis naprej (vsa soda stevila)
	printf("Izpisujemo tabelo naprej (vsa soda stevila) : \n");
	for( i=0; i<n; ++i )
	{
		if( tab[i]%2 == 0 )
			printf("%d ", tab[i]);
	}
	printf("\n");
 
	return 0;
}

