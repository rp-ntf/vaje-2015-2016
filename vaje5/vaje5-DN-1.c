/*
 * Tabele : 
 *
 * Domaca naloga : Tretje najvecje stevilo 
 * Sestavi program, ki 
 *	1. uporabnika najprej vpraša za največjo dolžino tabele (n)
 *	2. uporabnika vpraša za cela števila, ki jih shrani v to tabelo (tab)
 *  3. Izpise tretje najvecje stevilo v tabeli
 */

#include <stdio.h>

int main(void)
{
	int i; // števec
	int n; // velikost tabele 
	int max1; // majvecje stevilo v tabeli 
	int max2; // drugo najvecje stevilo v tabeli 
	int max3; // tretje najvecje stevilo v tabeli

	int MAX = 256; // najvecja dovoljena velikost tabele
	// Deklaracija tabele 
	int tab[MAX];
	// uporabljena velikost tabele :
	printf("Vnesite stevilo elementov tabele : ");
	scanf("%d", &n);
	if( n > MAX ) // Tabela ne sme biti večja od MAX
		n = MAX;
	if( n < 3 )
		n = 3;
	for( i=0; i<n; ++i )
	{
		printf("Vnesite %d-to stevilo tabele [%d] : ", i, n);
		scanf("%d", &tab[i] );
	}

	// Dodaj algoritem
 
	printf("Tretje najvecje stevilo v tabeli je %d\n", max3 );

	return 0;
}

