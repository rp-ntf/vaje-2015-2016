/*
 * Tabele : 
 *
 * Naloga : (Stetje stevila sodih stevil)
 * Sestavi program, ki 
 *	1. uporabnika najprej vpraša za največjo dolžino tabele (n)
 *	2. uporabnika vpraša za cela števila, ki jih shrani v to tabelo (tab)
 *	3. Izpise stevilo sodih stevil v tabeli
 *
 * Dodatna naloga : 
 *	- program naj na koncu izpise se vsa stevila v tabeli, ki so deljiva s 3
 */

#include <stdio.h>

int main(void)
{
	int i; // števec
	int n; // velikost tabele 
	int stevec; // stevilo sodih stevil
	int MAX = 256; // najvecja dovoljena velikost tabele
	// Deklaracija tabele 
	int tab[MAX];
	// uporabljena velikost tabele :
	printf("Vnesite stevilo elementov tabele : ");
	scanf("%d", &n);
	if( n > MAX ) // Tabela ne sme biti večja od MAX
		n = MAX;
	for( i=0; i<n; ++i )
	{
		printf("Vnesite %d-to stevilo tabele [%d] : ", i, n);
		scanf("%d", &tab[i] );
	}

	stevec = 0;
	for( i=0; i<n; ++i )
	{
		if( tab[i] % 2 == 0 )
			stevec++;
	}

	printf("V tabeli je %d sodih stevil.\n", stevec ); 

	// Izpis samo stevil, deljivih s 3
	printf("Izpis stevil, deljivih s 3.\n");
	for( i=0; i<n; ++i )
	{
		if( tab[i] % 3 == 0 )
			printf("%d ", tab[i]);
	}

	return 0;
}

