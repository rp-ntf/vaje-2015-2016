/*
 * zanke, ponavljanje
 *
 * Naloga : binomski koeficient
 * Sestavi program, ki uporabnika vpraša za stevili n in k, 
 * in izracuna binomski koeficient ( n nad k ).
 *
 * n nad k = n! / (k ! * (n-k)! )
 * n nad k = ( n * (n-1) * (n-2) * (n-3) * ... * (k+1) ) / ( 1 * 2 * 3 * ... * (n-k) )
 *			= n/1 * (n-1)/2 * (n-2)/3 * ... * (k+1)/(n-k)
 */

#include <stdio.h>

int main(void)
{
	int n;
	int k;
	int i; // stevec v zanki
	int r; // rezultat
	printf("Vnesite stevilo n : ");
	scanf("%d", &n);
	printf("Vnesite stevilo k : ");
	scanf("%d", &k);
	r = 1;
	for( i=1; i<=(n-k);  i++ )
	{
		printf("Clen %d je : %d / %d = %f\n", i, (n-i+1), i, ((float)(n-i+1))/i ); 
		r = r * (n-i+1) / i;
	}
	printf("%d nad %d = %d\n", n, k, r );

	return 0;
}

