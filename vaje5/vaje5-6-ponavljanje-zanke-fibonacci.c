/*
 * Ponavljanje : zanke
 *
 * Naloga : (Fibonacijevo zaporedje)
 * Sestavi program, ki izpise prvih n clenov fibonacijevega zaporedja.
 *
 * Fibonacijevo zaporednje je definirano z naslednjim predpisom : 
 *
 * f_1 = 1
 * f_2 = 1
 * f_n = f_(n-1) + f_(n-2)
 *
 */

#include <stdio.h>

int main(void)
{
	int f_n, f_nm1, f_nm2; // Zaporedni cleni fibonacijevega zaporedja 
	int max_n; // Stevilo izpisanih clenov 
	int n; // stevec zanke po clenih
	printf("Vnesite stevilo clenov zaporedja : ");
	scanf("%d", &max_n);
	// Zacnemo zaporedje pri n=3 :
	n = 3;
	f_nm1 = 1; // n-1 = 2, f_2 = 1
	f_nm2 = 1; // n-2 = 1, f_1 = 1
	printf("%d %d ", f_nm2, f_nm1); // Izpisemo prve clene zaporedja 
	do {
		// Izracunamo naslednji clen zaporedja
		f_n = f_nm1 + f_nm2;
		// ga izpismo
		printf("%d ",f_n);
		// in pripravimo f_nm1, f_nm2 za naslednji korak
		f_nm2 = f_nm1;
		f_nm1 = f_n;
		// ( zamenjamo : f_nm1 -> f_n, f_nm2 -> f_nm1 )
		// in povecamo stevec clenov zaporedja 
		n++;
	} while( n <= max_n );

	printf("\n"); 

	return 0;
}

