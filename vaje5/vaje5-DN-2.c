/*
 * Tabele : 
 *
 * Domaca naloga : Stetje stevil po ostankih
 * Sestavi program, ki 
 *	1. uporabnika najprej vpraša za največjo dolžino tabele (n)
 *	2. uporabnika vpraša za cela števila, ki jih shrani v to tabelo (tab)
 *  	3. uporabnika vprasa za stevilo, katerega ostanki pri deljenju ga zanimajo (m)
 *  	4. Izpise stevilo stevil v tabeli, ki imajo dolocen ostanek pri deljenju s stevilom m.
 *
 *  Za tabelo 
 *  1 2 3 6 8 11 
 *  in m = 3, program izpise 
 *  ostanek | stevilo stevil 
 *  0 : 2
 *  1 : 1 
 *  2 : 3
 *  
 */

#include <stdio.h>

int main(void)
{

	return 0;
}

