/*
 * Tabele : 
 *
 * Naloga : (Stetje stevila elementov)
 * Sestavi program, ki 
 *	1. uporabnika najprej vpraša za največjo dolžino tabele (n)
 *	2. uporabnika vpraša za cela števila, ki jih shrani v to tabelo (tab)
 *	3. uporabnika vpraša za števila (m), in prešteje, kolikokrat se to število (m) pojavi v tabeli (tab) 
 *		Vnos se zaključi z vpisom števila 0.
 */

#include <stdio.h>

int main(void)
{
	int i; // števec
	int n; // velikost tabele 
	int m; // Iskanjekano število 
	int stevec; // stevilo iskanih stevil
	int MAX = 256; // najvecja dovoljena velikost tabele
	// Deklaracija tabele 
	int tab[MAX];
	// uporabljena velikost tabele :
	printf("Vnesite stevilo elementov tabele : ");
	scanf("%d", &n);
	if( n > MAX ) // Tabela ne sme biti večja od MAX
		n = MAX;
	for( i=0; i<n; ++i )
	{
		printf("Vnesite %d-to stevilo tabele [%d] : ", i, n);
		scanf("%d", &tab[i] );
	}

	m = 1;
	while ( m != 0 )
	{
		stevec = 0;
		printf("Vpisite iskano stevilo : ");
		scanf("%d", &m);
		for( i=0; i<n; ++i )
		{
			if( tab[i] == m )
				stevec++;
		}
		printf("Stevilo %d se v tabeli pojavi %d-krat.\n", m, stevec );
	}

	return 0;
}

