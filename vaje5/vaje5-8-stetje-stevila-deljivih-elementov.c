/*
 * Tabele : 
 *
 * Naloga : (Stetje stevila deljivih elementov)
 * Sestavi program, ki 
 *	1. uporabnika najprej vpraša za največjo dolžino tabele (n)
 *	2. uporabnika vpraša za cela števila, ki jih shrani v to tabelo (tab)
 *	3. uporabnika vpraša za števila (m), in prešteje, koliko elemntov tabele (tab) je deljivo s tem stevilom (m)
 *		Vnos se zaključi z vpisom števila 0.
 */

#include <stdio.h>

int main(void)
{
	int i; // števec
	int n; // velikost tabele 
	int m; // Iskanjekano število 
	int stevec; // stevilo iskanih stevil
	int MAX = 256; // najvecja dovoljena velikost tabele
	// Deklaracija tabele 
	int tab[MAX];
	// uporabljena velikost tabele :
	printf("Vnesite stevilo elementov tabele : ");
	scanf("%d", &n);
	if( n > MAX ) // Tabela ne sme biti večja od MAX
		n = MAX;
	for( i=0; i<n; ++i )
	{
		printf("Vnesite %d-to stevilo tabele [%d] : ", i, n);
		scanf("%d", &tab[i] );
	}

	m = 1;
	while ( 1 )
	{
		stevec = 0;
		printf("Vpisite iskano stevilo : ");
		scanf("%d", &m);
		if( m == 0 )
			break;
		for( i=0; i<n; ++i )
		{
			if( tab[i] % m == 0 )
				stevec++;
		}
		printf("V tabeli je %d stevil, deljivih s stevilom %d.\n", stevec, m );
	}

	return 0;
}

