/*
 * Funkcije : ponavljanje
 *
 * Napiši program, ki z bisekcijo najde vsaj eno ničlo funkcije 
 *
 * f(x) = arctan(x/2) + 1.25 * sin(x)
 *
 * Graf funkcije je prilozen.
 *
 * Metoda bisekcije najde ničlo na intervalu [a,b], ki ga poda uporabnik. 
 * Na vsakem koraku metoda naredi sledeče : 
 *	1. izbere vmesno točko c = (a+b) / 2
 *	2. izračuna f(c) 
 *	3. Iz vrednosti f(c) določi ali je ničla funkcije na intervalu [a,c] ali [c,b]
 *	4. Skrči interval (v primeru da je ničla na [a,c]) : a -> a, b -> c
 * Metoda ponavlja vse korake, dokler ni interval z ničlo dovolj majhen (|a-b] < EPS).
 * Vrednost EPS vzamemo za 1e-3.
 *
 * Glej tudi : 
 * https://en.wikipedia.org/wiki/Bisection_method
 *
 * Za primeren zacetni interval glej prilozen graf funkcije.
 *
 * Dodatna naloga : 
 * namesto opisane funkcije poisci niclo polinoma, katerega koeficiente najprej preberes od uporabnika.
 */

#include <stdio.h>
#include <math.h>

float f( float x ) 
{
	return 0.0;
}

int main(void)
{
	float a, b, c;
	float fa, fb, fc;
	float EPS = 1e-3;
	printf("Vnesite začeten interval.\n");
	printf("a = ");
	scanf("%f", &a);
	printf("b = ");
	scanf("%f", &b);
    fa = f(a);
    fb = f(b);

	do {
		// izračunamo c
		c = (a+b)/2;
		// izračunamo f(c)
		fc = f(c);
		// ugotovimo, ali funkcija menja predznak na intervalu [a,c], ali [b,c] : 
		// Namig : če funkcija na intervalu [a,b] menja predznak, je fa*fb <= 0
		//
		// Na koncu še nastavimo meje intervala na nove meje intervala.
	} while ( (b-a) > EPS );

	return 0; 
}

