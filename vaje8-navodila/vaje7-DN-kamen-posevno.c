// Domaca naloga 3 : 
// Naredite funkcijo, ki vrne kako dalec prileti kamen, ki ga s hitrostjo v0 
// vrzemo pod kotom 45 stopinj 

// Privzamemo : 
// g = 10 (tezni pospesek)
// Zracnega upora ni.

#include <stdio.h>
#include <math.h>

float cas_padanja( float v0 ) 
{
	float g = 10;
	// Enacba za cas dviganja : 
	// v0 = g * t
	// Cas padanja pa je enak casu dviganja 
	// Sledi : 
	// t = 2 * v0 / g
	return 2 * v0 / g;
}

float dolzina_posevnega_meta( float v0 ) 
{
	float g = 10;
	// Hitrost razdelimo na navpicno in vodoravno komponento.
	float v_navpicno = v0 / sqrt(2.0);
	float v_vodoravno = v0 / sqrt(2.0);
	
	// Navpicni del gibanja je enak kot prej. 
	// Torej kamen pade na tla po enakem casu : 
	float cas_meta = cas_padanja( v_navpicno );

	// Kako dalec pa kamen prileti v tem casu v vodoravni smeri ? 
	return v_vodoravno * cas_meta;
}

int main( void ) 
{
	float v0; 
	v0 = 5;
	printf("Ce kamen vrzemo posevno (45o) s hitrostjo %f, potem pade na tla cez %f m.\n", v0, dolzina_posevnega_meta(v0) );

	v0 = 10;
	printf("Ce kamen vrzemo posevno (45o) s hitrostjo %f, potem pade na tla cez %f m.\n", v0, dolzina_posevnega_meta(v0) );

	v0 = 20;
	printf("Ce kamen vrzemo posevno (45o) s hitrostjo %f, potem pade na tla cez %f m.\n", v0, dolzina_posevnega_meta(v0) );


	// Najhitrejsi pitch pri baseballu : 106 mph = 47.39
	// https://www.youtube.com/watch?v=ngubly4hpHw
	v0 = 47.39;
	printf("(Baseball : najhitrejsi met)\n");
	printf("Ce kamen vrzemo posevno (45o) s hitrostjo %f, potem pade na tla cez %f m.\n", v0, dolzina_posevnega_meta(v0) );
	
	// Servis pri tenisu : 73.17 m/s
	// https://en.wikipedia.org/wiki/Fastest_recorded_tennis_serves
	v0 = 73.17;
	printf("(Tenis : najhitrejsi servis)\n");
	printf("Ce kamen vrzemo posevno (45o) s hitrostjo %f, potem pade na tla cez %f m.\n", v0, dolzina_posevnega_meta(v0) );
	//
	// Brcanje zoge pri nogometu : 14.9 m/s
	// http://www.livestrong.com/article/441962-how-much-force-does-an-average-soccer-player-kick-the-ball-with/
	v0 = 14.9;
	printf("(Nogomet : povprecen igralec)\n");
	printf("Ce kamen vrzemo posevno (45o) s hitrostjo %f, potem pade na tla cez %f m.\n", v0, dolzina_posevnega_meta(v0) );

	return 0;
}
