#include <stdio.h>
#include <math.h>
#include <string.h>


// Funkcije od prejsnic : 

int vsota( int a, int b ) 
{
	return a+b;
}

void razlicneOperacije( int a, int b )
{
	printf("%d + %d = %d\n", a, b, a+b );
	printf("%d - %d = %d\n", a, b, a-b );
	printf("%d * %d = %d\n", a, b, a*b );
	printf("%d / %d = %d\n", a, b, a/b );
}

// Nove funkcije : 
// Naloge : 
// Napisite funkcijo, ki 
//	- vrne vsoto sin(x) + cos(x)

float vsota_sin_cos( float x ) 
{
	return 0.0; 
}

//	- vrne exp(-x), hkrati pa izpise na zaslon vrednost exp( +x )

float vaja_exp( float x )
{
	// printf( ... ??? )
	// return (...) ?
	return 0.0;
}

//	- prebere vrednost realnega stevila od uporabnika, na zaslon pa izpise kvadrat prebranega stevila 
//	- izracuna polinom 0*x + -3 * x + 7 * x^2 + 22 * x^3
//	- izracuna polinom 12*x + 3 * x + 17 * x^2 + 2 * x^3
//	- vrne nakljucno celo stevilo v danem intervalu (namig : za poljubno nakljucno stevilo uporabi funkcijo rand() )
int nakljucno_stevilo_v_intervalu( int min_x, int max_x ) 
{
	// Vrnemo lahko min_x, min_x +1, ... max_x-1
	return rand();
}

//	- pove stevilo v sredini treh stevil, podanih kot argumenti

int main(void) 
{
	return 0;
}
