#include <stdio.h>
#include <string.h>
#include <math.h>

/* 
 * Strukture : uvod 
 *
 */

// [1]
struct tocka { // <- ime strukture
	float x; // <- element strukture
	float y; // <- se en element strukture
}; // <- na za zaklepajem je ";"

// [2] 
struct imenovana_tocka {
	float x;
	float y;
	char ime[256];
};

float razdalja_med_tockami( struct tocka t1, struct tocka t2 )
{
	float dx = t1.x - t2.x;
	float dy = t1.y - t2.y;
	return sqrt( dx*dx + dy*dy );
}

struct rezultat_kolokvija {
	char ime[256];
	int vpisna_stevilka;
	int tocke_naloga_1;
	int tocke_naloga_2; 
	int tocke_naloga_3;
};

struct vnos_imenik {
	char ime[256];
	int telefon;
};

void izpisi_vnos_imenik( struct vnos_imenik vnos )
{
	printf("%30s : %10d\n", vnos.ime, vnos.telefon );
}

void je_imenovana_tocka_v_krogu( struct imenovana_tocka t, struct imenovana_tocka sredisce, float R )
{
	float dx = t.x - sredisce.x;
	float dy = t.y - sredisce.y;
	float d2 = dx*dx + dy*dy;
	if( d2 < R * R ) 
		printf( "Tocka \"%s\" je znotraj kroga s srediscem v tocki \"%s\" in radijem %f\n", t.ime, sredisce.ime, R );
	else 
		printf( "Tocka \"%s\" je zunaj kroga s srediscem v tocki \"%s\" in radijem %f\n", t.ime, sredisce.ime, R );
};

int main( void ) 
{
	int i;
	/*
	 * Struktura v programskem jeziku je način grupiranja podatkov.
	 * S pomocjo struktur, lahko skupaj predstavimo vec razlicnih podatkov. 
	 *
	 * Na primer : za tocko v ravnini, lahko njeni koordinati (x,y) skupaj 
	 * predstavimo v strukturi [1].
	 */
	struct tocka t;
	t.x = 1.2;
	t.y = 2.3;
	printf("Tocka : x = %f, y = %f\n", t.x, t.y );
	/*
	 * Struktura tocka tako predstavlja tocko v ravnini, 
	 * in vsebuje dva elementa - realni stevili, ki predstavljata 
	 * x in y koordinati tocke.
	 *
	 * Elemente strukture dosezemo z operatorjem "." : <ime strukture>.<ime elementa>
	 * Elemente izpisujemo in nastavljamo kot vse spremenljivke enakega tipa.
	 */

	/* 
	 * V strukturo lahko spravimo spremenljivke razlicnih tipov [2], 
	 * in tudi tabele.
	 */
	struct imenovana_tocka t2;
	t2.x = 11.1;
	t2.y = 22.2;
	strcpy(t2.ime, "t2" );
	printf("Tocka %s : x = %f, y = %f\n", t2.ime, t2.x, t2.y );

	float R = 5;
	struct imenovana_tocka sredisce, t3, t4;
	strcpy( sredisce.ime, "sredisce kroga" );
	sredisce.x = 5;
	sredisce.y = 5;
	strcpy( t3.ime, "t3" );
	t3.x = 7.0;
	t3.y = 4.0;
	strcpy( t4.ime, "t4" );
	t4.x = 7.0;
	t4.y = 14.0;
	/* 
	 * Strukture, katerih elementi ne vsebujejo kazalcev, 
	 * lahko podamo v funkcije kot obicajne elementarne spremenljivke. 
	 */
	je_imenovana_tocka_v_krogu( t3, sredisce, R );
	je_imenovana_tocka_v_krogu( t4, sredisce, R );

	/* 
	 * Podobno kot lahko naredimo tabelo elementarnih spremenljivk, lahko naredimo tudi tabelo 
	 * struktur.
	 */
	struct tocka tabela_tock[10];
	tabela_tock[0].x = 1.0; 
	tabela_tock[0].y = 2.0;
	tabela_tock[5].x = 11.0; 
	tabela_tock[5].y = 12.0;

	struct vnos_imenik prijatelji[6];
	strcpy( prijatelji[0].ime, "Janez" );
	strcpy( prijatelji[1].ime, "Micka" );
	strcpy( prijatelji[2].ime, "Klemen" );
	strcpy( prijatelji[3].ime, "Miha" );
	strcpy( prijatelji[4].ime, "Mojca" );
	strcpy( prijatelji[5].ime, "Tine" );
	prijatelji[0].telefon = 10;
	prijatelji[1].telefon = 11;
	prijatelji[2].telefon = 12;
	prijatelji[3].telefon = 13;
	prijatelji[4].telefon = 14;
	prijatelji[5].telefon = 15;
	printf("Telefonske stevilke mojih prijateljev so :\n");
	for( i=0; i<5; ++i )
		izpisi_vnos_imenik( prijatelji[i] );

	return 0;
}
