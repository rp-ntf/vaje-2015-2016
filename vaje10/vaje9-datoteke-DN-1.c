/*
 * Delo z datotekami : Domaca naloga
 */

#include <stdio.h>

/* 
 * Naloga : 
 * - sestavite funkcijo, ki zapise tabelo realnih stevil v datoteko. 
 *   void pisi_v_datoteko( char ime_datoteke[], float * tab )
 *   {
 *		FILE * f;
 *		f = fopen( ime_datoteke, "rt");
	 // KOT NA VAJAH
 *   }
 * - sestavite funkcijo, ki iz datoteke prebere tabelo realnih stevil.
 */

void zapisi_tabelo( char * ime_datoteke, float * tab, int n )
{
	printf("Zapisujem tabelo v datoteko %s\n", ime_datoteke );
	FILE * f = fopen( ime_datoteke, "wt" );
	if ( f == NULL ) 
	{
		printf("Napaka pri odpiranju datoteke %s\n", ime_datoteke );
		return ;
	}
	int i; 
	for ( i=0; i<n; ++i )
	{
		fprintf( f, "%20.20f\n", tab[i] );
	}
	fclose(f);
}

void preberi_tabelo( char * ime_datoteke, float * tab, int n )
{
	printf("Berem tabelo iz datoteke %s\n", ime_datoteke );
	FILE * f = fopen( ime_datoteke, "rt" );
	if ( f == NULL ) 
	{
		printf("Napaka pri odpiranju datoteke %s\n", ime_datoteke );
		return ;
	}
	int i; 
	for ( i=0; i<n; ++i )
	{
		fscanf( f, "%f\n", &tab[i] );
	}
	fclose(f);
}


int main(void) 
{
	float tab[6] = { 1.2, 2.3, 3.4, 4.56789012344567, 5.6, 6.7 };
	int n = 6; // = sizeof( tab ) / sizeof( tab[0] );
	float tab2[6];
	int i;

	zapisi_tabelo( "dn-zapis-tabele.txt", tab, n );
	preberi_tabelo( "dn-zapis-tabele.txt", tab2, n );
 
	printf("Zapisano \t -> \t Prebrano\n");
	for( i=0; i<n; ++i )
	{
		printf("%20.20f \t -> \t %20.20f \n", tab[i], tab2[i] );
	}
	return 0;
}

