// - S strukturo predstavite cas v dnevu (ure in minute)
// - Napisite funkcijo, ki izracuna razliko med dvema casoma v minutah
#include <stdio.h>
#include <math.h>
#include <string.h>

struct cas_v_dnevu {
	int ure;
	int minute;
};

int razlika_cas_v_dnevu( struct cas_v_dnevu a, struct cas_v_dnevu b )
{
	int razlika = 0;
	razlika = 60 * ( a.ure - b.ure ) + ( a.minute - b.minute );
	return razlika;
}
// - S strukturo predstavite cas v letu - leto, mesec in dan
// - Napisite funkcijo, ki izpise podan cas v ISO formatu (YYYY-MM-DD)
struct cas_v_letu {
	int leto; 
	int mesec; 
	int dan;
};
void izpisi_cas_v_letu( struct cas_v_letu t )
{
	// YYYY-MM-DD
	printf("%d-", t.leto );
	if ( t.mesec < 10 )
		printf("0");
	printf("%d-", t.mesec );
	if ( t.dan < 10 )
		printf("0");
	printf("%d\n", t.dan );
}
// - Definirajte strukturo, ki predstavlja ucilnico na fakulteti (ime stavbe, nadstropje, stevilka ucilnice)
struct ucilnica {
	char stavba[256];
	int nadstropje;
	int stevilka_ucilnice;
};
// - Napisite funkcijo, ki izpise informacije o dani ucilnici
void izpisi_ucilnico( struct ucilnica u )
{
	printf("Stavba : %s\n", u.stavba );
	printf("Nadstropje : %d\n", u.nadstropje );
	printf("Stevilka ucilnice : %d\n", u.stevilka_ucilnice );
}

// - Definirajte strukturo, ki predstavlja avtomobil (znamka, model, poraba goriva)
struct avtomobil {
	char znamka[256];
	char model[256];
	float poraba_goriva;
};

// - Napisite funkcijo, ki izpise podatke o avtomobilu
void izpisi_opis_avtomobila( struct avtomobil a )
{
	printf("Avtomobil :\n");
	printf("Znamka : %s\n", a.znamka );
	printf("Model  : %s\n", a.model  );
	printf("Poraba goriva : %f l / 100 km\n", a.poraba_goriva );
}

int main( void ) 
{
	struct avtomobil a; 
	strcpy( a.znamka, "Zavod Crvena Zastava in Kragujevac" );
	strcpy( a.model,  "Zastava 750 - fico" );
	a.poraba_goriva = 6.2;
	izpisi_opis_avtomobila( a );

	struct ucilnica u;
	strcpy( u.stavba, "Askrceva" );
	u.nadstropje = 3;
	u.stevilka_ucilnice = 22;
	izpisi_ucilnico( u );

	struct cas_v_dnevu t1, t2;
	t1.ure = 20;
	t1.minute = 35;
	t2.ure = 19;
	t2.minute = 15;
	printf("Razlika casov je %d minut.\n", razlika_cas_v_dnevu( t1, t2 ) );
	struct cas_v_letu t3;
	t3.leto = 2015;
	t3.mesec = 8;
	t3.dan = 3;
	izpisi_cas_v_letu( t3 );

	return 0;
}
