#include <stdio.h>
/*
 * Vaje 10 : ponavljanje : zanke, funkcije, datoteke
 */
// Napisite funkcijo, ki v podano datoteko izpise stevila od 1 do 10 po vrsticah #
// 1
// 2
// 3
// ...
// 10
void izpisi_stevila( char * ime_datoteke )
{
	FILE * f; 
	f = fopen( ime_datoteke, "wt" );
	if( f == NULL )
	{
		printf("Tezava pri odpiranju datoteke\n");
		return;
	}
	int i;
	for( i=0; i<=10; ++i )
		fprintf( f, "%d\n", i );
	fclose(f);
}
// Napisite funkcijo, ki v datoteko izpise drevo iz zvezdic. Ime datoteke in velikost drevesa funkcija dobi kot argument 
// Primer drevesa (n = 4) : 
// *
// **
// ***
// ****
void izpisi_drevo_zvezdic( char * ime_datoteke, int n )
{
	FILE * f; 
	f = fopen( ime_datoteke, "wt" );
	if( f == NULL )
	{
		printf("Tezava pri odpiranju datoteke\n");
		return;
	}
	int i, j;
	for( i=1; i<=n; ++i )
	{
		for( j=0; j<i; ++j )
			fprintf(f,"*");
		fprintf(f, "\n");
	}
	fclose(f);
}
int main( void )  
{
	// izpisi_stevila("izpis_stevil.txt");
	izpisi_drevo_zvezdic("drevo_zvezdic.txt", 10);
	return 0;
}
// DOMACI NALOGI : 
// 1. Napisite funkcijo, ki iz datoteke prebere tekst in ga izpise na zaslon.
// 2. Napisite funkcijo, ki prebere datoteko s stevili (po vrsticah) in vrne vsoto prebranih stevil.
// Primer vhodne datoteke : 
// 1.2
// 2.3
// 3.4 
// funkcija vrne 6.9

