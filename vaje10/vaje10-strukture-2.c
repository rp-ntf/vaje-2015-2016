/*
 * Strukture : osnove 
 */
#include <stdio.h>
#include <math.h>
// Naloga : 
// 
// Definirajte strukture za nastete primere, 
// in jih uporabite na kratkem primeru, ki izpise vsebino strukture
//
// - S strukturo predstavite kompleksno stevilo #
struct kompleksno_stevilo {
	float re;
	float im;
};
// - napisite funkcijo, ki vrne argument (kot) kompleksnega stevila #
float argument_kompleksnega_stevila( struct kompleksno_stevilo z )
{
	return atan2( z.im, z.re );
}
// - napisite funkcijo, ki vrne absolutno vrednost kompleksnega stevila 
float absolutna_vrednost_kompleksnega_stevila( struct kompleksno_stevilo z ) 
{
	return sqrt( z.im * z.im + z.re * z.re );
}
// - napisite funkcijo, ki izracuna kompleksno konjugirano stevilo danemu stevilu #
void konjugirano_stevilo( struct kompleksno_stevilo z, struct kompleksno_stevilo * p_konj )
{
	(*p_konj).re = z.re;
	(*p_konj).im = 0 - z.im;
}
// - napisite funkcijo, ki sesteje dani dve kompleksni stevili (rezulat vrne preko kazalca)
void vsota( struct kompleksno_stevilo z1, struct kompleksno_stevilo z2, struct kompleksno_stevilo * p_vsota )
{
	(*p_vsota).re = z1.re + z2.re;
	(*p_vsota).im = z1.im + z2.im;
}
// - napisite funkcijo, ki odsteje dani dve kompleksni stevili (rezultat vrne preko kazalca)
int main( void )
{
	struct kompleksno_stevilo z1, z2, konj_z1, z_vsota, z_razlika;
	z1.re = 1.1;
	z1.im = 2.2;
	z2.re = 3.3;
	z2.im = 5.5;
	printf("Kompleksno stevilo z1 : %f + %f i\n", z1.re, z1.im );
	printf("Kompleksno stevilo z2 : %f + %f i\n", z2.re, z2.im );
	printf("Kot kompleksnega stevila : %f\n", argument_kompleksnega_stevila( z1 ) );
	printf("Absolutna vrednost kompleksnega stevila : %f\n", absolutna_vrednost_kompleksnega_stevila( z1 ) );
	konjugirano_stevilo( z1, & konj_z1 );
	printf("Konjugirano kompleksno stevilo : %f + %f i\n", konj_z1.re, konj_z1.im );
	vsota( z1, z2, & z_vsota );
	printf("Vsota kompleksnih stevil : %f + %f i\n", z_vsota.re, z_vsota.im );
	return 0;
}
 
