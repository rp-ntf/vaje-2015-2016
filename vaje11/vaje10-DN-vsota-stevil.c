// 2. Napisite funkcijo, ki prebere datoteko s stevili (po vrsticah) in vrne vsoto prebranih stevil.
// Primer vhodne datoteke : 
// 1.2
// 2.3
// 3.4 
// funkcija vrne 6.9

#include <stdio.h>

float vsota_stevil( char * ime_datoteke )
{

	char vrstica[256];
	FILE * f;
	float stevilo;
	float vsota = 0;
	f = fopen( ime_datoteke, "rt" );
	if ( f == NULL )
	{
		printf("Napaka pri odpiranju datoteke : %s\n", ime_datoteke );
		return 0;
	}
	while( fscanf( f, "%f", &stevilo ) != EOF )
	{
		vsota += stevilo;
	}
	fclose(f);
	return vsota;
}

int main( void )
{
	printf("Vsota stevil v datoteki je %f.\n", vsota_stevil("dn-vsota-stevil.txt" ) );
	return 0;
}
