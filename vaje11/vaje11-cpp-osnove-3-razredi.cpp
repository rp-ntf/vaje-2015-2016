/*
 * Vaje 11 : C++ - razredi 
 */

#include <iostream>

/*
 * Glaven dodatek jezika C++ v primerjavi z jezikom C 
 * pa so razredi. 
 * Razred je objekt, ki lahko vsebuje 
 * - podatkovne tipe (kot strukture)
 * - metode 
 */

class vektor2D
{
	public : // Metode in elementi za oznako "public" so javne, in jih lahko klicemo od kjerkoli.
		// Konstruktor razreda je posebna metoda, 
		// ki naredi razred. 
		vektor2D( float x, float y )
		{ 
			_x = x;
			_y = y;
			std::cout << "Prvi konstruktor : " << std::endl;
			izpisi();
		}
		vektor2D( float x )
		{
			_x = x;
			_y = 0;
			std::cout << "Drugi konstruktor : " << std::endl;
			izpisi();
		}
		~vektor2D()
		{
			std::cout << "Destruktor : " << std::endl;
			izpisi();
		}
		// Razred ima lahko poljubno mnogo metod
		void izpisi() 
		{
			std::cout << "Vektor ( " << _x << ", " << _y << " )" << std::endl;
		}
		void pristej_vektor( vektor2D v )
		{
			pristej_vektor_x( v );
			pristej_vektor_y( v );
		}
	private : // Metode in elementi za oznako "private" so vidni le znotraj razreda.
		void pristej_vektor_x( vektor2D v )
		{
			_x += v._x;
		}
		void pristej_vektor_y( vektor2D v )
		{
			_y += v._y;
		}
		float _x;
		float _y;
};

int main( void ) 
{
	vektor2D v1( 2, 3 ), v2( 10 ), v3( 4, 5 );
	v1.izpisi();
	v2.izpisi();
	v3.izpisi();
	std::cout << "Vektorju v2 pristejemo v1 : " << std::endl;
	v2.pristej_vektor( v1 );
	v2.izpisi();
	return 0;
}
