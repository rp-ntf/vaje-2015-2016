// DOMACI NALOGI : 
// 1. Napisite funkcijo, ki iz datoteke prebere tekst in ga izpise na zaslon.

#include <stdio.h>

void preberi_tekst( char * ime_datoteke )
{
	char vrstica[256];
	FILE * f;
	f = fopen( ime_datoteke, "rt" );
	if ( f == NULL )
	{
		printf("Napaka pri odpiranju datoteke : %s\n", ime_datoteke );
		return;
	}
	while( fgets( vrstica, 255, f ) != NULL )
	{
		printf("%s", vrstica );
	}
	fclose(f);
}

int main( void )
{
	preberi_tekst( "dn-beri-tekst.txt" );
	return 0;
}
