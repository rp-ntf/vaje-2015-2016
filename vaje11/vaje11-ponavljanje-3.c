// Naloga 2 : 
// definirajte strukturo "vremenske_razmere", ki vsebuje : 
// - dan 
// - mesec 
// - leto (cas opisanih vremenskih razmer) : celo stevilo 
// - opis vremena ("snezi", "dezuje", "soncno" ) : niz
// - temperatura : realno stevilo 
// - padavine v mm : realno stevilo 
// Napisite funkcijo, ki primerno izpise to strukturo 
//
// Naloga 2.5 :
// definirajte strukturo "novoletna_vecerja", ki vsebuje : 
// - opis hrane ("t-bone") : niz
// - opis pijace ("kokta") : niz
// - kolicino popite pijace : realno stevilo 
// Napisite funkcijo, ki primerno izpise to strukturo 

#include <stdio.h>
#include <string.h>

struct vremenske_razmere {
	int dan; 
	int mesec; 
	int leto; 
	char opis_vremena[256];
	float temperatura;
	float padavine;
};

void izpisi_vremenske_razmere( struct vremenske_razmere vreme )
{
	printf( "Dan : %d\n", vreme.dan );
	printf( "Mesec : %d\n", vreme.mesec );
	printf( "Leto : %d\n", vreme.leto );
	printf( "Opis vremena : %s\n", vreme.opis_vremena );
	printf( "Temperatura : %f C\n", vreme.temperatura );
	printf( "Padavine : %f mm\n", vreme.padavine );
}

int main( void ) 
{
	struct vremenske_razmere v;
	v.dan = 10;
	v.mesec = 22;
	v.leto = 2014;
	strcpy( v.opis_vremena, "oblacno" );
	v.temperatura = -5.4;
	v.padavine = 1.456;

	izpisi_vremenske_razmere( v );

	return 0;
}
