// Naloga 1 # : 
// definirajte strukturo "darilo", ki predstavlja darilo pod novoletno smerekico. 
// Struktura naj vsebuje : 
// - ime prejemnika darila (npr: janez) : niz
// - opis darila (npr: premog, avtomobil, ... ) : niz
// - tezo darila (da dedek mraz lahko izracuna obremenitev sank) : realno stevilo 
// Napisite funkcijo, ki primerno izpise to strukturo, ki ji jo podamo kot argument.

#include <stdio.h>
#include <string.h>

struct darilo {
	char prejemnik[256];
	char opis[256];
	float teza;
};

void izpisi_darilo( struct darilo d1 ) 
{
	printf("Prejemnik darila : %s\n", d1.prejemnik );
	printf("Opis darila : %s\n", d1.opis );
	printf("Teza darila : %f\n", d1.teza );
}

int main( void ) 
{
	struct darilo d1, d2;
	strcpy( d1.prejemnik, "janez" );
	strcpy( d1.opis, "avtomobilcek" );
	d1.teza = 1.5;

	strcpy( d2.prejemnik, "miha" );
	strcpy( d2.opis, "vlakec" );
	d2.teza = 10.1;

	izpisi_darilo( d1 );
	izpisi_darilo( d2 );

	return 0;
}

