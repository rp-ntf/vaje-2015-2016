// Naloga 3 # : 
// s pomocjo zanke sestejte vsa stevila med 1 in N (N preberete od uporabnika), 
// ki niso deljiva z 2 ali 5.

#include <stdio.h>

int main( void )
{
	int N;
	int vsota = 0;
	int i;
	printf("Vnesite N : \n");
	scanf( "%d", &N );
	for( i=1; i<=N; ++i )
	{
		if( 
				(i % 2 == 0)
				||
				(i % 5 == 0)
		  )
		{
		}
		else 
		{
			vsota += i;
		}
	}
	printf( "Vsota = %d\n", vsota );
	return 0;
}

