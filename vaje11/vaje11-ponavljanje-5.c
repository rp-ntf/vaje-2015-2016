// Naloga 4 : 
// s pomocjo zanke sestejte vsa stevila med 1 in N (N preberete od uporabnika), 
// ki so deljiva s 3, a ne s 7
//
// Naloga 5 : 
// s pomocjo zanke sestejte vsa stevila med 1 in N (N preberete od uporabnika), 
// ki niso deljiva z 3, 5, 7, 9 ali 15

#include <stdio.h>

int main( void )
{
	int N;
	int vsota = 0;
	int i;
	printf("Vnesite N : \n");
	scanf( "%d", &N );
	for( i=1; i<=N; ++i )
	{
		if( 
				(i % 3 == 0)
				&&
				(i % 7 != 0)
		  )
		{
			vsota += i;
		}
		else 
		{
		}
	}
	printf( "Vsota = %d\n", vsota );
	return 0;
}
