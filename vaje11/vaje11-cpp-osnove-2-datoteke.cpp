/*
 * Vaje 11 : 
 * C++ : pisanje v datoteko / branje iz datoteke 
 */

#include <iostream>

/*
 * V programskem jeziku C++ 
 * pisemo v datoteke s pomocjo datotecnih tokov.
 * Datotecni tokovi se obnasajo kot standardni vhodni in izhodni tokovi, 
 * dobimo pa jih v knjiznici
 */
#include <fstream>

int main( void )
{
	std::ofstream f_out;
	f_out.open("datoteke_cpp.txt", std::ios_base::out ); 
	// datotecni tok odpremo podobno kot datoteko v jeziku C.
	// Prvi argument funkcije "open", ki je del razreda "std::ofstream" je  
	// ime datoteke, drugi argument pa je kombinacija oznak za nacin odpiranja datoteke, 
	// ki jih zdruzimo skupaj s pomocjo binarnega "OR" operatorja "|".
	//
	// std::ios_base::in = odpremo za branje 
	// std::ios_base::out = odpremo za pisanje 
	// std::ios_base::ate = odpremo za pisanje na konec datoteke (lahko pa kasneje pisemo na zacetek datoteke)
	// std::ios_base::app = odpremo za dodajanje (vsako pisanje se izvede na konec datoteke)
	// std::ios_base::trunc = ko odpremo datoteko, pobrisemo njeno dosedanjo vsebino (truncate)

	for( int i=0; i<10; ++i )
		f_out << i << "\n";
	// In na koncu datoteko se zapremo
	f_out.close();

	// Branje iz datoteke poteka podobno :
	std::ifstream f_in;
	f_in.open("datoteke_cpp.txt", std::ios_base::in );
	int x;
	while( true )
	{
		f_in >> x;
		if( f_in.eof() == true ) 
			break;
		std::cout << "Prebrali smo stevilo : " << x << std::endl;
	}

	return 0;
}

