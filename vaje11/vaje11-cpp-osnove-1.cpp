/*
 * Vaje 11 : 
 * osnove jezika C++
 */

/*
 * Jezik C++ je bil razvit kot nadgradnja jezika C, 
 * za to lahko v njem uporabljamo sintakso jezika C (skoraj vedno), 
 * z dodatnimi moznostmi, ki jih prinasa nov jezik. 
 *
 * Funkcija "main" je lahko povsem enaka. 
 *
 * Datoteke z izvorno kodo v jeziki C++ imajo obicajno koncnico ".cpp" (C : ".c").
 */

#include <iostream>
// Namesto funkcij iz knjiznice <stdio.h>, 
// v jeziku C++ uporabimo funkcije iz knjiznice <iostream>.
// Izpis na zaslon in branje podatkov od uporabnika v jeziku C++ 
// naredimo s pomocjo vhodno / izhodnih tokov

using namespace std;

int main( void )  
{
	// cout je izhodni tok (standard output). 
	// std::cout pomeni, da je cout del imenskega prostora std, za to do njega dostopamo 
	// z std::cout 
	std::cout << "Pozdravljen, svet !" << std::endl;
	// Objekt std::endl pa zakljuci izpis. 
	// Tocneje : izpise prehod v novo vrstico in zagotovi, 
	// da se niz izpise v tem trenutku.
	int celo_stevilo = 1234;
	float realno_stevilo = 1.234567;
	char niz[] = "NIZ ZNAKOV"; 
	
	// Izpisovanje na zaslon s pomocjo tokov izvedemo s pomocjo vhodnega operatorja "<<".
	// Ta operator v kombinaciji z izhodnim tokom lahko izpise vse osnovne podatkovne tipe, 
	// brez posebnih dolocil. 
	// Npr : 
	std::cout << "Celo stevilo : " << celo_stevilo << std::endl;
	std::cout << "Realno stevilo : " << realno_stevilo << std::endl;
	std::cout << "Niz znakov : " << niz << std::endl;

	// Branje podatkov od uporabnika izvedemo s pomocjo izhodnega operatorja ">>" 
	// v kombinaciji s tokom "std::cin" (standard input)
	int prebrano_celo_stevilo;
	float prebrano_realno_stevilo;

	std::cout << "Vpisite celo stevilo : " << std::endl;
	std::cin >> prebrano_celo_stevilo;
	std::cout << "Prebrali smo celo stevilo " << prebrano_celo_stevilo << std::endl;
	std::cout << "Vpisite realno stevilo : " << std::endl;
	std::cin >> prebrano_realno_stevilo;
	std::cout << "Prebrali smo realno stevilo " << prebrano_realno_stevilo << std::endl;

	return 0;
} 

// Glej tudi : 
// https://slo-tech.com/clanki/04009/
