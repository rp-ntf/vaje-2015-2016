/*
 * Naloga : Funkcije
 * Sestavite program / funkcijo, ki z rekurzijo izračuna izraz n!.
 *
 * n! = n * (n-1) * (n-2) * ... * 1
*/

#include <stdio.h>
#include <string.h>

int fakulteta( int n )
{
	// Ce je n<=1 vrnemo 1, ce ni klicemo fakulteta(n-1)
}

int main(void)
{
	printf("%d ! = %d, pravilen rezultat = %d\n", 6, fakulteta(6), 720 );
	printf("%d ! = %d, pravilen rezultat = %d\n", 4, fakulteta(4), 24 );
	printf("%d ! = %d, pravilen rezultat = %d\n", 7, fakulteta(7), 5040 );

	return 0;  
}

