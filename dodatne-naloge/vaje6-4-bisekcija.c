/*
 * Zanke : ponavljanje
 * Napiši program, ki z bisekcijo najde vsaj eno ničlo funkcije 
 *
 * f(x) = 12*x^5 + 17*x^4 - 2*x^3 - 205*x^2 - 7 * x + 402
 *
 * Metoda bisekcije najde ničlo na intervalu [a,b], ki ga poda uporabnik. 
 * Na vsakem koraku metoda naredi sledeče : 
 *	1. izbere vmesno točko c = (a+b) / 2
 *	2. izračuna f(c) 
 *	3. Iz vrednosti f(c) določi ali je ničla funkcije na intervalu [a,c] ali [c,b]
 *	4. Skrči interval (v primeru da je ničla na [a,c]) : a -> a, b -> c
 * Metoda ponavlja vse korake, dokler ni interval z ničlo dovolj majhen (|a-b] < EPS).
 * Vrednost EPS vzamemo za 1e-3.
 *
 * Glej tudi : 
 * https://en.wikipedia.org/wiki/Bisection_method
 */

#include <stdio.h>
#include <math.h>

int main(void)
{
	float a, b, c;
	float fa, fb, fc;
	float EPS = 1e-3;
	printf("Vnesite začeten interval.\n");
	printf("a = ");
	scanf("%f", &a);
	printf("b = ");
	scanf("%f", &b);
    fa = 12*pow(a,5.0) + 17*pow(a,4.0) - 2*pow(a,3.0) - 205*pow(a,2.0) - 7 * a + 402;
    fb = 12*pow(b,5.0) + 17*pow(b,4.0) - 2*pow(b,3.0) - 205*pow(b,2.0) - 7 * b + 402;

	do {
		// izračunamo c
		c = (a+b)/2;
		// izračunamo f(c)
		fc = 12*pow(c,5.0) + 17*pow(c,4.0) - 2*pow(c,3.0) - 205*pow(c,2.0) - 7 * c + 402;
		// ugotovimo, ali funkcija menja predznak na intervalu [a,c], ali [b,c] : 
		// Namig : če funkcija na intervalu [a,b] menja predznak, je fa*fb <= 0
		//
		// Na koncu še nastavimo meje intervala na nove meje intervala.
	} while ( (b-a) > EPS );

	return 0; 
}

