/*
 * Nizi : osnove
 * Domaca naloga : 
 * Sestavi program, ki v podanem nizu zamenja velike crke z malimi. 
 * Na primer, ce uporabnik vnese 
 * "To je PrIMer"
 * program izpise 
 * "tO JE pRimER"
 */

#include <stdio.h>
#include <string.h>

int main(void)
{
	int MAX = 256;
	char niz[MAX];
	int i;
	// najprej preberemo niz od uporabnika (gets)
	printf("Vnesite niz : ");
	gets(niz);
	printf("Prebrali smo (gets) niz \"%s\".\n", niz);
	// Potem pa v zanki gremo skozi niz, in vsak majhen znak zamenjamo z velikim znakom.
	// for( i=0; i<strlen(niz); ++i )
	// 	printf("%2d : [%3i] - %c\n", i, niz[i], niz[i] );
	for( i=0; i<strlen(niz); ++i )
	{
		if( 'A' <= niz[i] && niz[i] <= 'Z' )
		{
			// niz[i] je velika crka 
			niz[i] = niz[i] + 'a' - 'A'; 
		}
		else if( 'a' <= niz[i] && niz[i] <= 'z' )
		{
			// niz[i] je mala crka
			niz[i] = niz[i] - 'a' + 'A';
		}
	}
	printf("Spremenjen niz je : \"%s\".\n", niz);
	// for( i=0; i<strlen(niz); ++i )
	// 	printf("%2d : [%3i] - %c\n", i, niz[i], niz[i] );


	return 0; 
}

