/*
 * Nizi : nadaljevanje
 * Naloga : 
 */

#include <stdio.h>

int main(void)
{
	int MAX = 256;
	int i; // stevec
	char * p; // kazalec, več o tem naslednjič

	// ###################################################
	// ######### Nadaljevanje ############################
	// ###################################################
	
	// STRSTR 
	// #include <string.h> 
	// char * strstr( char tekst[], char iskanNiz[] )
	//
	// Funkcija strstr poisce niz "iskanNiz" v nizu "tekst". 
	// Vrne kazalec na najden niz oziroma NULL ce niza ne najde. 
	char niz5[MAX];
	char niz6[MAX];
	printf("Vnesite niz [ tekst ] : ");
	gets(niz5);
	printf("Vnesite iskan niz : ");
	gets(niz6);
	p = strstr( niz5, niz6 );
	printf("Iskali smo niz \"%s\" v nizu \"%s\", ", niz6, niz5 );
	if ( p == NULL )
		printf("in ga nismo nasli.\n");
	else 
		printf("in ga nasli na indeksu %ld.\n", p-niz5 );

	return 0;  
}

