/*
 * Nizi : utrjevanje
 * Naloga : 
 * Sestavite program, ki bo vizualno prestel stevilo crk v nizu.
 * Najprej od uporabnika prebere niz (celo vrstico), 
 * potem pa izpise stevilke 0-9 namesto ustreznih crk v nizu, 
 * na vsakem koraku izpisa eno crko vec.
 *
 * Primer izpisa programa, kjer uporabnik vpise niz "marko skace po zeleni" : 
 
Vnesite niz : marko skace po zeleni
Prebrali smo niz "marko skace po zeleni".
  0 : 0arko skace po zeleni
  1 : 01rko skace po zeleni
  2 : 012ko skace po zeleni
  3 : 0123o skace po zeleni
  4 : 01234 skace po zeleni
  5 : 012345skace po zeleni
  6 : 0123456kace po zeleni
  7 : 01234567ace po zeleni
  8 : 012345678ce po zeleni
  9 : 0123456789e po zeleni
 10 : 01234567890 po zeleni
 11 : 012345678901po zeleni
 12 : 0123456789012o zeleni
 13 : 01234567890123 zeleni
 14 : 012345678901234zeleni
 15 : 0123456789012345eleni
 16 : 01234567890123456leni
 17 : 012345678901234567eni
 18 : 0123456789012345678ni
 19 : 01234567890123456789i
 20 : 012345678901234567890
 
*/

#include <stdio.h>

int main(void)
{
	int MAX = 256;
	int i; // stevec
	char niz[MAX];

	// Najprej od uporabnika preberemo niz :
	printf("Vnesite niz : ");
	gets(niz);
	printf("Prebrali smo niz \"%s\".\n", niz);

	// Potem pa znake v nizu postopoma nadomestimo z stevilkami : 
	// for( i=0; i<??; ++i )
	// {
	// niz[i] = ?
	// printf(?)
	// }

	return 0;  
}

