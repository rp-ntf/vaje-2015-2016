/*
 * Tabele : 2D
*/

#include <stdio.h>

int main(void)
{
	// vecdimenzionalne tabele so definirane kot obicajne tabele, 
	// le da za vsako dimenzijo obseg tabele nastavimo v ustreznem paru [].
	
	// 1D tabela z 10 elementi:
	int tab1D1[10];
	// 1D tabela z 5 elementi : 
	int tab1D2[] = { 1, 2, 3, 4, 5 };

	// 2D tabela z 3 x 3 elementi : 
	int tab2D1[3][3];
	// 2D tabela z 4x2 elemnti 
	int tab2D2[4][2] = { { 1,2 }, {3,4}, {5,6}, {7,8} };
	// Ce ob definiciji vrednosti elemntom tudi pripisemo, 
	// moramo paziti na to, da je struktura gnezdenja { } 
	// enaka kot so dimenzije tabele
	
	// 3D tabela z 2x3x5 elemnti : 
	int tab3D1[2][3][5];
	// 3D tabela z 4x3x2 elemnti : 
	int tab3D2[4][3][2] = {
		{ 
			{1,2},
			{3,4},
			{5,6}
		},
		{ 
			{1,2},
			{3,4},
			{5,6}
		},
		{ 
			{1,2},
			{3,4},
			{5,6}
		},
		{ 
			{1,2},
			{3,4},
			{5,6}
		}
	};
	
	// izpis 2D tabele : 
	int tab[4][2] = { { 1,2 }, {3,4}, {5,6}, {7,8} };
	int i,j;
	printf("Izpisujem tabelo 4x2 :\n");
	for( i=0; i<4; ++i )
	{
		for( j=0; j<2; ++j )
		{
			printf(" % 4d", tab[i][j]);
		}
		printf("\n");
	}

	// prirejanje vrednosti elementom : 
	for( i=0; i<4; ++i )
		tab[i][0] = 100*i;

	printf("Izpisujem tabelo 4x2 (po prirejanju vrednosti):\n");
	for( i=0; i<4; ++i )
	{
		for( j=0; j<2; ++j )
		{
			printf(" % 4d", tab[i][j]);
		}
		printf("\n");
	}


	return 0;  
}

