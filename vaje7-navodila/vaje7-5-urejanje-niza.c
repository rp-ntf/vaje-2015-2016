/*
 * Nizi : utrjevanje
 * Naloga : 
 * Sestavite program, ki bo uredil od uporabnika prebran niz. 
 * Na vsakem koraku urejanja, naj program zamenja vsaki sosednji crki, ki nista urejeni po velikostnem redu, 
 * korak urejanja pa naj ponovi n krat, kjer je n dolzina vnesenega niza.
 *
 * Na vsakem koraku naj program izpise trenuten niz.
 *
 * Primer izpisa programa, kjer uporabnik vpise niz "Makro Skace" : 
 
*/

#include <stdio.h>

int main(void)
{
	int MAX = 256;
	int i; // stevec
	int j; // notranji stevec
	int n; // dolzina niza
	char niz[MAX];
	char zac; // zacasna spremenljivka

	// Najprej od uporabnika preberemo niz :
	printf("Vnesite niz : ");
	gets(niz);
	printf("Prebrali smo niz \"%s\".\n", niz);

	// V zunanji zanki urejanje niza ponovimo n-krat 
	// for( i=0;  ... )
	// {
	//		// V notranji zanki gremo čez niz in zamenjamo 2 zaporedna znaka, če le ta nista urejena zaporedno
	//		for( j=0; ... )
	//		{
	//			if( niz[i-1] > niz[i] ) 
	//			{
	//				// zamenjamo
	//			}
	//		}
	// }

	return 0;  
}

