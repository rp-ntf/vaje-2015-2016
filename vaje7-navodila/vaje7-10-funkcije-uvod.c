/*
 * Funkcije : uvod
 *
*/

#include <stdio.h>
#include <string.h>

/*
 * Do zdaj smo že spoznali kar nekaj funkcij : 
 *  - printf 
 *  - scanf
 *  - pow 
 *  - log 
 *  - gets 
 *  - strlen 
 *  - strcpy 
 *  - strstr 
 * 
 * Funkcija main je posebna funkcija, 
 * ki vsebuje glaven del programa. 
 *
 * Tako kot smo do zdaj v vsakem programu definirali funkcijo main, 
 * lahko definiramo tudi druge nove funkcije, in jih potem kličemo iz drugih delov programa.
 *
 * Definicijo funkcije razumemo tako : 
 *
 * int main(void) // funkcija main vrne spremenljivko tipa int (od tu int pred besedo main),  
 *	ne sprejme pa nobenih argumentov (od tu beseda void v oklepajih)
 * {
 * 
 * 	return 0;  // ta stavek pove, da bo ta funkcija main vrnila spremenljivko tipa int z vrednostjo 0
 * }
 * 
 * Primer funkcije, ki sesteje 2 celi stevili :
 */
int vsota( int a, int b ) 
{
	return a+b;
}

/*
 * Funkcije lahko znotraj sebe kličejo tudi druge funkcije, npr printf. 
 * Primer funkcije, ki izračuna produkt, kvocient, vsoto in razliko dveh celih stevil in jih izpise, 
 * vrne pa ne nicesar : 
 */
void razlicneOperacije( int a, int b )
{
	printf("%d + %d = %d\n", a, b, a+b );
	printf("%d - %d = %d\n", a, b, a-b );
	printf("%d * %d = %d\n", a, b, a*b );
	printf("%d / %d = %d\n", a, b, a/b );
	// nima return stavka, ker je to funkcija tipa void
}

/*
 * Lahko pa funkcija kliče tudi samo sebe (rekurzivno). 
 * Na primer, izračun fibonacijevih stevil bi lahko implementirali tudi tako :
 */
int fibbonaci( int n ) 
{
	if( n < 1 ) 
		return 0; // za indekse manjse od 1, nasa funkcija vraca 0
	else if( n==1 )
		return 1; // f_1 = 1
	else if( n==2 ) 
		return 1; // f_2 = 1
	else 
		return fibbonaci(n-1) + fibbonaci(n-2); // sicer vrnemo vsoto prejsnih dveh stevil 
}

/*
 * V splosnem je funkcija definirana kot : 

<tip spremenljivke, ki ga vrne> ime_funkcije( <tip argumenta1> argument1, <tip argumenta2> argument2, ... )
{
... telo funkcije, kjer je funkcija implementirana 
	return <izraz, ustrezen tipu spremenljivke, ki jo funkcija vrača>;
}

 * Funkcije lahko kot parameter sprejmejo tudi tabelo. 
 * Primer funkcije, ki presteje stevilo znakov v podanem nizu : 
*/

int prestejSteviloZnakov( char znak, char niz[] ) 
{
	int stevec = 0;
	int i;
	for( i=0; i<strlen(niz); ++i )
		if( niz[i] == znak ) 
			stevec++;
	return stevec;
}

/* 
 * Funkcije lahko klicejo tudi druge funkcije. 
 * Primer funkcije, ki presteje stevilo samoglasnikov : 
 */

int prestejSamoglasnike( char niz[] ) 
{
	char samoglasniki[] = "aeiou";
	int stevec = 0;
	int i;
	for ( i=0; i<strlen(samoglasniki); ++i )
		stevec += prestejSteviloZnakov( samoglasniki[i], niz );
	return stevec;
}

int main(void)
{
	int i;
	printf("Funkcija vsota : %d + %d = %d\n", 3, 5, vsota(3,5) );
	printf("Funkcija vsota : %d + %d = %d\n", 2, 7, vsota(2,7) );
	printf("Funkcija vsota : %d + %d = %d\n", 4, 10, vsota(4,10) );

	printf("Funkcija razlicneOperacije : \n");
	razlicneOperacije( 3, 5 );
	razlicneOperacije( 2, 7 );
	razlicneOperacije( 4, 10 );

	printf("Funkcija fibbonaci :\n");
	for( i=1; i<10; ++i )
		printf("fibbonaci( %d ) = %d\n", i, fibbonaci( i ) );

	char niz[] = "Marko skace po zeleni trati";
	printf("Stevilo znakov '%c' v nizu \"%s\" je %d.\n", 'a', niz, prestejSteviloZnakov( 'a', niz ) );
	printf("Stevilo znakov '%c' v nizu \"%s\" je %d.\n", 'o', niz, prestejSteviloZnakov( 'o', niz ) );

	printf("Stevilo samoglasnikov v nizu \"%s\" je %d.\n", niz, prestejSamoglasnike(niz) );

	return 0;  
}

