/*
 * Nizi : utrjevanje
 * Naloga : 
 * Sestavite program, 
 * ki od uporabnika najprej prebere število nizov, 
 * potem prebere toliko nizov, 
 * jih uredi po abecednem redu, 
 * in na koncu izpiše tabelo nizov urejeno po vrstnem redu.
 *
 * Namig : 
 * algoritem je lahko enak, kot za urejanje enega samega niza, 
 * le da za primerjavo nizov in urejanje nizov uporabimo funkciji strcmp in strcpy.
 *
*/

#include <stdio.h>

int main(void)
{
	int MAX = 256;
	int i; // stevec
	int j; // notranji stevec
	int n; // stevilo nizov
	char niz[MAX];
	char tabelaNizov[MAX][MAX];
	char zac; // zacasna spremenljivka
	printf("Vnesite stevilo nizov : ");
	scanf("%d", &n);
	for( i=0; i<n; ++i )
	{
		printf("Vnesite niz %3d : ", i );
		gets(tabelaNizov[i]);
	}

	// algoritem je enak kot za urejanje enega niza, le da tokrat urejamo elemente tabele nizov

	return 0;  
}

