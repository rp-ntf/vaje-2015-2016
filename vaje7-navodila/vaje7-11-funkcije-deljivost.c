/*
 * Naloga : Funkcije
 * Sestavite funkcijo, 
 * ki najde stevilo skupnih deljiteljev obeh stevil.
 * Skupne deljitelje naj sproti tudi izpisuje.
 *
 * Program naj preveri, ali funkcija zares pravilno deluje za nekaj stevil, in izpise rezulat
*/

#include <stdio.h>
#include <string.h>

int steviloDeljiteljev( int a, int b )
{
	int stevec = 0;
	int i;
	int minAB = a < b ? a : b ;
	// najpreprosteje je, da gremo v zanki od 1 do manjsega od stevil (a,b) 
	// in stejemo, koliko stevil deli obe stevili hkrati.
	return stevec;
}

int main(void)
{
	int seznamA[] = { 4, 8, 7, 14, 16, 15 };
	int seznamB[] = { 2, 6, 4,  4,  8, 21 };
	int seznamN[] = { 2, 2, 1,  2,  4,  2 };
	int n = 6;
	int i;
	int nDeljiteljev;
	for( i=0; i<n; ++i )
	{
		nDeljiteljev = steviloDeljiteljev( seznamA[i], seznamB[i] );
		printf("Stevilo skupnih deljiteljev stevil %d in %d je %d, pravilna vrednost je %d ", seznamA[i], seznamB[i], nDeljiteljev );
		if ( nDeljiteljev == seznamN[i] )
			printf("[PRAVILNO]\n");
		else 
			printf("[NEPRAVILNO]\n");
	}

	return 0;  
}

