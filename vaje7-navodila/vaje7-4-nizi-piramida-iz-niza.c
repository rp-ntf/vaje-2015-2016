/*
 * Nizi : utrjevanje
 * Naloga : 
 * Sestavite program, ki bo izpisal narobe obrnjeno piramido iz crk niza, 
 * ki ga poda uporabnik.
 *
 * Primer izpisa programa, kjer uporabnik vpise niz "PIRAMIDA" : 


Vnesite niz : PIRAMIDA
Prebrali smo niz "PIRAMIDA".

PIRAMIDA
 IRAMID 
  RAMI  
   AM   
 
 * Primer izpisa programa, kjer uporabnik vpise niz "MARKO SKACE" : 

Vnesite niz : MARKO SKACE
Prebrali smo niz "MARKO SKACE".

MARKO SKACE
 ARKO SKAC 
  RKO SKA  
   KO SK   
    O S    
 
*/

#include <stdio.h>

int main(void)
{
	int MAX = 256;
	int i; // stevec
	char niz[MAX];

	// Najprej od uporabnika preberemo niz :
	printf("Vnesite niz : ");
	gets(niz);
	printf("Prebrali smo niz \"%s\".\n", niz);
	printf("\n");

	// V zanki gremo cez cel niz, 
	// in na vsakem koraku zamenjamo prvo in zadnjo crko z znakom ' '

	return 0;  
}

